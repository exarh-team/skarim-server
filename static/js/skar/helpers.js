/**
Support
*/

function addUrl(path, title, data) {
    if (typeof data === "undefined") data = null
    // trim slashes
    path = trim(path, ['/']);
    // new page
    if (path != window.location.pathname.slice(1))
        window.history.pushState(data, title, window.location.protocol+'//'+host+'/'+path);
}

function replaceUrl(path, title, data) {
    if (typeof data === "undefined") data = window.history.state();
    // trim slashes
    path = trim(path, ['/']);
    // change page
    window.history.replaceState(data, title, window.location.protocol+'//'+host+'/'+path);
}

function openFromUrl() {
    path = window.location.pathname;
    path = path.slice(1).split("/");
    switch(path[0]) {
        case '':
        case 'myprofile':
            Skar.openMyProfile();
            break;
        case 'search':
            Skar.openSearchContact(window.history.state);
            break;
        default:
            if (path[0][0] in ['@', '~', '+'])
                login = 'skaro::'+host+'/'+path[0];
            else
                login = path.join('/')

            Skar.openDialog(login, true);

            /* Пример. Тут это лишнее
            if (path.length > 1)
                switch(path[1]) {
                    case 'info':
                        Skar.openDialogInfo(login)
                        break;
                    case 'profile':
                        Skar.openUserProfile(login);
                        break;
                }
            */
            break;
    }
}

function trim(str, chars) {
    if (typeof chars === "undefined") chars = [' '];

    chars = chars.join('\\');

    return str.replace('/^['+chars+']+/', '').replace('/['+chars+']+$/', '');
}

/* Конвертирует обьект формы
[{"name":somename,"value":somevalue},{},{},{}] в {"somename":somevalue,,,,}
*/
jQuery.fn.toDict = function() {
    var fields = this.serializeArray();
    var json = {}
    for (var i = 0; i < fields.length; i++) {
        json[fields[i].name] = fields[i].value;
    }
    if (json.next) delete json.next;
    return json;
};

/* Генератор уникальных ID */
function IDGenerator(prefix){
    this.prefix = prefix;
    this.last_num = 0;
    this.getID = function(){
        var candidate = (+new Date());
        if (candidate <= this.last_num)
            candidate = this.last_num + 1;
        this.last_num = candidate;
        return this.prefix + this.last_num;
    }
}
Uniq = new IDGenerator('');

/* Эмуляция "запрос-ответ"
*
* function Request({
*     timeout: (int),
*     success: (callback),
*     error: (callback),
*     always: (callback),
*     data: somedata
* })
*/
function Request(s) {
    if (typeof s.timeout === "undefined") s.timeout = 10;
    if (typeof s.data !== "undefined") this.data = s.data;

    this.succes_do = s.success
    this.error_do = s.error
    this.always_do = s.always

    var self = this;
    this.success = function(request) {
        console.timeEnd("Query "+self.request_id);

        if (typeof self.succes_do !== "undefined") self.succes_do(request);
        if (typeof self.always_do !== "undefined") self.always_do("success", request);

        clearTimeout(self.request_id);
        delete Skar.requests[self.request_id];
    }
    this.error = function(code) {
        console.timeEnd("Query "+self.request_id);

        if (typeof self.error_do !== "undefined") self.error_do(code);
        if (typeof self.always_do !== "undefined") self.always_do("error", code);

        clearTimeout(self.request_id);
        delete Skar.requests[self.request_id];
    }

    this.request_id = setTimeout(self.error,s.timeout*1000);
    Skar.requests[this.request_id] = this;

    console.time("Query "+self.request_id);
}

/* Рисует в favicon количество непрочитанных диалогов */
function DrawFavicon(unread)
{
    unread = parseInt(unread);
    var canvas = document.createElement('canvas');
    canvas.width = canvas.height = 16;
    var ctx = canvas.getContext('2d');
    var img = new Image();
    img.src = '/img/im.png';

    img.onload = function() {
        ctx.drawImage(img, 0, 0);

        if ((unread > 0) & (unread < 10)) {
            ctx.drawImage(img, 0, -16*unread);
        } else if (unread >= 10) {
            ctx.drawImage(img, 0, -160);
        }

        var link = document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = canvas.toDataURL("image/x-icon");
        document.getElementsByTagName('head')[0].appendChild(link);
    }
}

/* Отправляет браузерное оповещение о новом сообщении */
function SendNotification(options)
{
    options = $.extend({
        title: "",
        icon: "/img/logo_40.png",
        body: "",
        tag: "",
        login: false
    }, options);

    if ('Notification' in window) {
        notification = new Notification(options.title, {
            icon: options.icon,
            body: options.body,
            tag: options.tag
        });
    } else if ('mozNotification' in navigator) {
        notification = navigator.mozNotification.createNotification(options.title, options.body, options.icon);
    } else if (notificationsMsSiteMode) {
        window.external.msSiteModeClearIconOverlay();
        window.external.msSiteModeSetIconOverlay('img/icons/icon16.png', options.title);
        window.external.msSiteModeActivate();
        notification = {
            index: idx
        };
    }
    else {
        return;
    }
    if (options.login) {
        notification.onclick = function() {
            window.focus();
            Skar.openDialog(options.login);
        }
    }
}

// Устанавливает куки
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

// Получает куки
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
// Удаляет куку
function deleteCookie(name) {
  setCookie(name, "", { expires: -1 })
}

// Обработчик - Акроним, чтобы при нажатии ENTER отправлялась форма, а при нажатии SHIFT+ENTER происходил перенос строки.
function ShiftEnterLine(e) {
    e = e || window.event;

    // Следующие 3сек нужно обновлять окно, т.к. будет происходить анимация
    DOM.reinitialise(3000,200);

    if (!e.shiftKey && e.keyCode == 13) {
        $(this).closest('form').submit();
        return false;
    }
    return true;
};
// Выводит уведомление
function show_notice(type, text, timeout, buttons) {
    if (typeof timeout == "undefined") timeout = false; else timeout *= 1000;
    if (typeof buttons == "undefined") buttons = false;

    var tmp_name = "noty"+type.charAt(0).toUpperCase()+type.substr(1);
    if (tmp_name in Template)
        text = Template[tmp_name](text);

    var n = noty({
        text        : text,
        type        : type,
        dismissQueue: true,
        layout      : 'topRight',
        closeWith   : ['click'],
        theme       : 'skaro',
        timeout     : timeout,
        maxVisible  : 10,
        animation   : {
            open  : 'animated bounceInRight',
            close : 'animated bounceOutRight',
            easing: 'swing',
            speed : 500
        },
        buttons   : buttons
        /* For buttons argument
        [
            {
                addClass: 'btn-green',
                text: 'Ok',
                onClick: function ($noty) {
                    // this = button element
                    // $noty = $noty element
                    $noty.close();
                    noty({force: true, text: 'Вы нажали "Ok"', type: 'success'});
                }
            },
            {
                addClass: 'btn-red',
                text: 'Cancel',
                onClick: function ($noty) {
                    $noty.close();
                    noty({force: true, text: 'Вы нажали "Cancel"', type: 'error'});
                }
            }
        ]*/
    });
    return n;
};

// Return GET params as array
function getUrlParams() {
    var res = {};
    if (window.location.search.length>0) {
        var url = window.location.search.match(/\?(.+)/)[1].split('&');
        url.forEach(function(entry){
            hash = entry.split('=');
            res[hash[0]] = hash[1];
        });
    }
    return res;
};


function activeCE(button, ce_blocks, save_func, fa) {
    if (typeof fa === "undefined") fa = [];

    $(button).parent().children().removeClass("hide");
    $(button).addClass("hide");

    ce_blocks.each(function() {

        var self = $(this),
            buttons = self.find(".editable-buttons"),
            field = self.find(".editable-value"),
            old_value = field.text(),
            field_name = self.attr("data-name");

        self.data("beforeValue", old_value);

        field.on("focusout", function() {
            var field = $(this),
                new_val = field.text();

            if (new_val != self.data("beforeValue")) {

                self.data("beforeValue", new_val);

                var firsts_args = fa.slice(),// copy array
                    obj = {};

                obj[field_name] = new_val;
                firsts_args.push(obj);

                // save data
                save_func.apply(Skar, firsts_args).done(function(info) {
                    if (typeof info !== "undefined" && info)
                        self.removeClass("invalid").addClass("valid").attr("data-success", info);
                    else
                        self.removeClass("invalid").addClass("valid");
                })
                .fail(function(info) {
                    self.removeClass("valid").addClass("invalid").attr("data-error", info);
                });

                // show back button
                buttons.find(".editable-buttons__back-button").removeClass("hide")
                .on("click", function() {
                    $(this).addClass("hide");
                    field.text(old_value);
                    field.trigger.call(field, "focusout");
                });
            }
        });
        field.on("keypress", function (e) {
            var key = e.which;
            if(key == 13) {  // the enter key code
                field.trigger.call(field, "focusout");
                return false;
            }
        });

        field.attr("ContentEditable", true);
        self.removeClass("disabled");
    });
}

function deactiveCE(button, ce_blocks) {
    $(button).parent().children().removeClass("hide");
    $(button).addClass("hide");

    ce_blocks.each(function() {
        var self = $(this),
            buttons = self.find(".editable-buttons"),
            field = self.find(".editable-value");

        field.off("focusout").off("keypress");
        field.attr("ContentEditable", false);
        self.removeClass("valid invalid").addClass("disabled");
        buttons.find("li").addClass("hide");
    });
}
