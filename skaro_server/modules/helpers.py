#!/usr/bin/env python

import httpagentparser
from random import randint

from modules import config


def unique(*args,**kargs):
    """
    Удаляет дубликаты в списке.
    если указать only_key равным ключу, существующему во всех элементах
    то сравнение будет идти только по содержимому этого ключа
    соответственно, при использовании only_key все элементы должны быть словарями
    """
    def gen(s, only_key=False):
        seen = set()
        for i in s:
            if not only_key: it = i
            else: it = i[only_key]

            if it not in seen:
                seen.add(it)
                yield i

    return list(gen(*args,**kargs))


def recursive_merge(d1, d2):
    """Обновляет первый словарь дополняя его вторым словарем рекурсивно"""
    d2 = d2.copy()
    for k, v in d1.items():
        if k in d2:
            # значения словарь?
            if all(isinstance(e, dict) for e in (v, d2[k])):
                d2[k] = recursive_merge(v, d2[k])
    d1.update(d2)
    return d1


def is_request(data):
    """Проверяет нужен ли сообщению клиента ответ сервера"""
    if "on_request" in data.keys() and type(data['on_request']) is int:
        return True
    else:
        return False


def is_exist_keys(dict_data, *keys):
    """Проверяет ключи в словаре на существование и пустоту"""
    for key in keys:
        try:
            v = dict_data[key]
            if not v:
                raise KeyError()
        except KeyError:
            return False
    return True


def filtering_user_data(data):
    """Очищает данные юзера от конфиденциальных данных, не копирует входящий обьект"""
    if 'password' in data.keys():
        del data['password']
    for login, settings in data['accounts'].items():
        if 'password' in settings.keys():
            del settings['password']

    return data

def filtering_conf_data(data):
    """Очищает данные о конференции от конфиденциальных данных, не копирует входящий обьект"""
    if 'password' in data.keys():
        del data['password']
    if 'members' in data.keys():
        del data['members']

    return data

def parse_login(login, and_parse_skaro_login=False, to_recover_with="@"):
    x = login.split('::', 1)

    if len(x) < 2:
        x = ["skaro", '/'.join(parse_skaro_login(x[0], to_recover_with))]

    if and_parse_skaro_login:
        x[1:] = parse_skaro_login(x[1], to_recover_with)
    else:
        x[0] = x[0].lower()

    return tuple(x)


def parse_skaro_login(login, to_recover_with="@"):
    x = login.split('/', 1)

    if len(x) < 2:
        x = [config.read("host"), x[0]]

    if x[1] and x[1][0] not in ("~", "@", "+"):
        x[1] = to_recover_with + x[1]

    x[0] = x[0].lower()

    return tuple(x)


def recover_full_login(login, with_symbol="@"):
    return "::".join(parse_login(login, to_recover_with=with_symbol))


def is_skaro_conf_login(full_login):
    (protocol, host, login) = parse_login(full_login, True, to_recover_with="~")

    if protocol == "skaro" and login[0] == "~":
        return True
    else:
        return False


def generate_device_id(self):
    return ' '.join(
        httpagentparser.simple_detect(
            self.request.headers["User-Agent"]
        )[:-1]
    ).replace(" ", "_") + "_" + str(randint(10**3, 10**4-1))
