#!/usr/bin/env python
#
#
# Validate module
# validating strings and e.t.c.
#
#

import logging
from re import error as ReException, compile, match, ASCII, DOTALL, IGNORECASE, LOCALE, MULTILINE, UNICODE, VERBOSE
from datetime import datetime

P_LOGIN = compile(r"^[\w\-]+$", flags=IGNORECASE | UNICODE)
P_MAIL = compile(r"^.+@.+\..+$", flags=IGNORECASE | UNICODE)
P_DEVICE_ID = compile(r"^[\w\-\_\.]+$", flags=IGNORECASE | UNICODE)

logger = logging.getLogger(__name__)  # named logging for validate.py

def is_valid(value, pattern):
    try:
        if match(pattern, value):
            return True
    except ReException:
        logger.error("Error in re.math(%s, %s)", str(pattern), str(value), exc_info=True)

    return False


def validate_args(func, use_accurate_informing=True):
    """
    Декоратор, осуществляющий проверку аргументов согласно схемам проверки уложенных в аннотации
    """
    def check(self, *args, **kwargs):
        pos = 0
        for name, checker in func.__annotations__.items():

            if name in kwargs:
                e = checker.check_data(kwargs.get(name))
            else:
                try:
                    arg = args[pos]
                    pos += 1
                except IndexError:
                    arg = None
                finally:
                    e = checker.check_data(arg)

            if e:
                data = {}
                if use_accurate_informing:
                    data = args[0]

                self.write_info(data, info=e)
                return


        return func(self, *args, **kwargs)

    return check

class DictSchema(object):
    """
    Create schema
    DictSchema(
        key_name = StringValue(required=True,min=10,max=20....),
        key_name_2 = ListValue(min=1, item=DictSchema(...),
        ...
    )
    for check "data" by using check_data(data) method
    that returned error string or False if "data" are valid
    """
    def __init__(self, **fields):

        self.fields = fields

    def check_data(self, data):
        if data is None:
            return " Значение должно быть указано"
        if type(data) is not dict:
            return " Значение должно быть ассоциативным массивом"
        if self.fields:
            # Проверка на лишние ключи
            for key in data.keys():
                if key not in self.fields.keys():
                    return ("['%s']" % (key,)) + "Лишний ключ"
            # Проверка ключей
            for key, check_obj in self.fields.items():
                s = check_obj.check_data(data.get(key))
                if s: return ("['%s']" % (key,)) + s

    def __repr__(self):
        return "DictSchema(" + ", ".join("%s = %s" % param for param in self.fields.items()) + ")"

class IntValue(object):
    def __init__(self, required=False, min=None, max=None):
        self.required = required
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not int:
            return " Значение должно быть целым числом"
        if not data and self.required:
            return " Значение должно быть заполнено"
        if data is not None:
            if self.min is not None and data < self.min:
                return " Значение не должно быть меньше числа %d" % (self.min,)
            if self.max is not None and data > self.max:
                return " Значение не должно быть больше числа %d" % (self.max,)

    def __repr__(self):
        return "IntValue("\
               + ", ".join("%s = %s" % param for param in
                            (("required",self.required),
                             ("min",self.min),
                             ("max",self.max)))\
               + ")"

class NumericValue(object):
    def __init__(self, required=False, min=None, max=None):
        self.required = required
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None \
                and (type(data) is not int or type(data) is not float):
            return " Значение должно быть числом"
        if not data and self.required:
            return " Значение должно быть заполнено"
        if data is not None:
            if self.min is not None and data < self.min:
                return " Значение не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and data > self.max:
                return " Значение не должно быть больше длины %d" % (self.max,)

    def __repr__(self):
        return "NumericValue("\
               + ", ".join("%s = %s" % param for param in
                            (("required",self.required),
                             ("min",self.min),
                             ("max",self.max)))\
               + ")"

class BoolValue(object):
    def __init__(self, required=False):
        self.required = required

    def check_data(self, data):
        if data is not None and type(data) is not bool:
            return " Значение должно быть либо True либо False"
        if not data and self.required:
            return " Значение должно быть заполнено"

    def __repr__(self):
        return "BoolValue(" + "%s = %s" % ("required",self.required) + ")"

class StringValue(object):
    def __init__(self, required=False, min=None, max=None, pattern=None, prepare_callback=None):
        self.required = required
        self.pattern = pattern
        self.prepare_callback = prepare_callback
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not str:
            return " Значение должно быть строкой"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data and self.prepare_callback is not None:
            data = self.prepare_callback(data)

        if data is not None:
            if self.min is not None and len(data) < self.min:
                return " Значение не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and len(data) > self.max:
                return " Значение не должно быть больше длины %d" % (self.max,)
            if data and self.pattern is not None and not is_valid(data, self.pattern):
                return " Значение не корректно"

    def __repr__(self):
        return "StringValue("\
               + ", ".join("%s = %s" % param for param in
                            (("required",self.required),
                             ("min",self.min),
                             ("max",self.max),
                             ("pattern",self.pattern)))\
               + ")"

class ListValue(object):
    def __init__(self, required=False, item=None, min=None, max=None):
        self.required = required
        self.item = item
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not str:
            return " Значение должно быть списком(массивом)"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data is not None:
            if self.min is not None and len(data) < self.min:
                return " Количество элементов не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and len(data) > self.max:
                return " Количество элементов не должно быть больше длины %d" % (self.max,)
            if self.item is not None:
                for i, item in enumerate(data):
                    s = self.item.check_data(item)
                    if s: return ("[%d]" % (i,)) + s

    def __repr__(self):
        return "ListValue("\
               + ", ".join("%s = %s" % param for param in
                            (("required",self.required),
                             ("item",self.item),
                             ("min",self.min),
                             ("max",self.max)))\
               + ")"

class DictValue(object):
    def __init__(self, required=False, keys=None, values=None, min=None, max=None):
        self.required = required
        self.keys = keys
        self.values = values
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not dict:
            return " Значение должно быть списком(массивом)"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data is not None:
            if self.min is not None and len(data) < self.min:
                return " Количество элементов не должно быть меньше длины %d" % (self.min,)
            if self.max is not None and len(data) > self.max:
                return " Количество элементов не должно быть больше длины %d" % (self.max,)
            if self.keys is not None or self.values is not None:
                for key, val in data.items():
                    if self.keys is not None:
                        s = self.keys.check_data(key)
                        if s: return ("[%d]" % (key,)) + s

                    if self.values is not None:
                        s = self.values.check_data(val)
                        if s: return ("[%s]" % (key,)) + s

    def __repr__(self):
        return "DictValue("\
               + ", ".join("%s = %s" % param for param in
                            (("required",self.required),
                             ("keys",self.keys),
                             ("values",self.values),
                             ("min",self.min),
                             ("max",self.max)))\
               + ")"

class DatetimeValue(object):
    def __init__(self, required=False, min=None, max=None):
        self.required = required
        self.min = min
        self.max = max

    def check_data(self, data):
        if data is not None and type(data) is not datetime.datetime:
            return " Значение должно быть валидной временной меткой"
        if not data and self.required:
            return " Значение должно быть заполнено"

        if data is not None:
            if self.min is not None and data < self.min:
                return " Значение не должно быть меньше даты %s" % (self.min,)
            if self.max is not None and data > self.max:
                return " Значение не должно быть больше даты %s" % (self.max,)

    def __repr__(self):
        return "NumericValue("\
               + ", ".join("%s = %s" % param for param in
                            (("required",self.required),
                             ("min",self.min),
                             ("max",self.max)))\
               + ")"
