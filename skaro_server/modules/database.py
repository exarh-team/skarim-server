#!/usr/bin/env python

import logging

import tornado.ioloop
import tornado.gen
from tornado.options import define, options
import psycopg2.extras
import psycopg2.extensions
import momoko
Json = psycopg2.extras.Json

from modules import helpers, config

# For catch exceptions in external try...except
#import psycopg2
#Warning = psycopg2.Warning
#Error = psycopg2.Error

logger = logging.getLogger(__name__)  # named logging for database.py

define("db_dsn", default="dbname=skarim user=postgres", type=str, help="database connection options")
define("db_size", default=1, type=int, help="count of database connections in pool")
define("db_max_size", default=10, type=int, help="max count database connections in pool")


# Подключает к БД, создает курсор и устанавливает кодировку UTF-8
def connect():
    global pool

    pool = momoko.Pool(
        options.db_dsn,
        size=options.db_size,
        max_size=options.db_max_size,
        #connection_factory=psycopg2.extras.DictConnection,
        cursor_factory=psycopg2.extras.RealDictCursor,
        ioloop=tornado.ioloop.IOLoop.instance()
    )
    
    pool.connect()

    pool.execute("SET CLIENT_ENCODING TO 'UTF8';\
                  SET NAMES 'UTF8';")

    return pool


def close():
    pool.close()


def query(*args, **kwargs):
    return pool.execute(
        *args, **kwargs
    )


# Сделана для галочки, использовать её воообще не следует
# Psycopg2 в каждом execute имеет удобный интерфейс,
# Который приводит типы и защищает от инъекций
def escape(var):
    return pool.mogrify("%s", (var,))


# Получает данные юзера
def getUserByLogin(login, **kwargs):
    return pool.execute(
        "SELECT * FROM users WHERE login = %s LIMIT 1",
        [login], **kwargs
    )
    # after use .fetchone()


# Получает данные юзера
def getUserByEMail(email, **kwargs):
    return pool.execute(
        "SELECT * FROM users WHERE email = %s LIMIT 1",
        [email], **kwargs
    )
    # after use .fetchone()

# Изменяет данные юзера
def setUser(data, search_by_login=None, **kwargs):

    # Вычленяем только существующие в таблице ключи
    user = {key: val for key, val in data.items() if key in (
        "login",
        "name",
        "password",
        "current_info",
        "contacts",
        "accounts"
    )}

    if search_by_login is None:
        user["__search_by_login__"] = helpers.parse_login(user.pop('login'), True)[2][1:]
    else:
        user['login'] = helpers.parse_login(user['login'], True)[2][1:]
        user["__search_by_login__"] = helpers.parse_login(search_by_login, True)[2][1:]

    if 'contacts' in user:
        user['contacts'] = Json(user['contacts'])
    if 'accounts' in user:
        user['accounts'] = Json(user['accounts'])

    return pool.execute(
        "UPDATE users SET "
        + ", ".join("%s = %s" % (key, "%("+key+")s") for key in user.keys() if key != "__search_by_login__")
        + " WHERE login = %(__search_by_login__)s",
        user, **kwargs
    )
    # after use try/except for status operation

# Добавляет или удаляет контакт пользователям, логины которых перечислены в logins
# Чтобы удалить контакт, нужно передать None (JSON null)
def addContactUsersByLogins(logins, contact, **kwargs):
    logins = [helpers.parse_login(login, True)[2][1:] for login in logins]
    return pool.execute(
        "UPDATE users\
            SET contacts = jsonb_merge(%(contact)s::jsonb, contacts::jsonb)::json\
            WHERE login = ANY (%(logins)s)",
        {
            "contact": Json(contact),
            "logins": logins
        }, **kwargs
    )
    # after use try/except for status operation

# Добавляет нового пользователя
def addUser(user, **kwargs):
    user["login"] = helpers.parse_login(user['login'], True)[2][1:]
    return pool.execute(
        "INSERT INTO users(login, password, email) VALUES (%(login)s,%(password)s,%(email)s)",
        user, **kwargs
    )
    # after use try/except for status operation

# Ищет пользователей по началу их логина
def searchContacts(search_str, self_login, **kwargs):
    return pool.execute(
        """
        SELECT
            concat('skaro::""" + config.read("host") + """/@', login) as login,
            name,
            current_info,
            tanimoto_cmp_strings(users.login, %s) as match_val
        FROM users
        WHERE users.login <> %s AND lower(users.login) LIKE lower(%s)
        ORDER BY match_val DESC
        """,
        [search_str, self_login, search_str+"%"], **kwargs
    )
    # after use .fetchall()

# Получает контакты юзера
def getFavorites(login, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    login = helpers.parse_login(login, True)[2][1:]

    return pool.execute(
        "SELECT contacts FROM users WHERE login = %s LIMIT 1",
        [login], **kwargs
    )
    # after use .fetchone()[0]


# Получает логины юзеров, у которых юзер(или конфа) с @login содержится в друзьях.
def getAtWhomInFavorites(login, is_conference=False, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = ColumnCursor(column_key=0)

    if is_conference:
        login = helpers.recover_full_login(login, with_symbol="~")
    else:
        login = helpers.recover_full_login(login)

    return pool.execute(
        """SELECT login FROM users WHERE %s IN
               (SELECT json_object_keys FROM json_object_keys(contacts))""",
        [login], **kwargs
    )
    # after use .fetchall()


def getChats(login, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = ColumnCursor(column_key=0)

    login = helpers.recover_full_login(login)

    return pool.execute(
        """
        SELECT DISTINCT ON (login) login FROM (
            (SELECT took AS login, removed FROM history WHERE sent = %(login)s)
            UNION
            (SELECT sent AS login, removed FROM history WHERE took = %(login)s)
        ) AS history WHERE %(login)s <> ALL(history.removed)
        """,
        {"login": login}, **kwargs
    )
    # after use .fetchall()


# Получает аккаунты юзера
def getAccounts(login, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    login = helpers.parse_login(login, True)[2][1:]

    return pool.execute(
        "SELECT accounts FROM users WHERE login = %s LIMIT 1",
        [login], **kwargs
    )
    # after use .fetchone()[0]


# Добавляет одно сообщение в историю
def addHistory(message, is_conference=False, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    message["from"] = helpers.recover_full_login(message["from"])
    message["to"] = helpers.recover_full_login(message["to"])

    if is_conference:
        table = "history_confs"
    else:
        table = "history"

    return pool.execute(
        "INSERT INTO "+table+"(sent, took, date, body) VALUES (%(from)s,%(to)s,%(date)s,%(body)s) RETURNING mid",
        message, **kwargs
    )
    # after use .fetchone()[0]


# Получает историю из диалога
def getHistory(sent, took, is_conference=False, **kwargs):
    with_symbol="@"
    if is_conference: with_symbol="~"

    sent = helpers.recover_full_login(sent, with_symbol=with_symbol)
    took = helpers.recover_full_login(took, with_symbol=with_symbol)

    if is_conference:
        table = "history_confs"
        query = "sent = %(one)s OR took = %(one)s"
    else:
        table = "history"
        query = "(sent = %(one)s AND took = %(two)s)\
                    OR (sent = %(two)s AND took = %(one)s)"

    return pool.execute(
        """
        SELECT * FROM """+table+""" WHERE
            %(two)s <> ALL("""+table+""".removed)
                AND ("""+query+""")
            ORDER BY date ASC
        """,
        {
            "one": sent,
            "two": took,
        }, **kwargs
    )
    # after use .fetchall()

@tornado.gen.coroutine
def clearHistory(login1, login2, is_conference=False, **kwargs):
    login1 = helpers.recover_full_login(login1)
    login2 = helpers.recover_full_login(login2)

    if is_conference:
        table = "history_confs"
    else:
        table = "history"

    update = yield pool.execute(
        """UPDATE """+table+""" SET removed = array_append(removed, %(one)s)
            WHERE %(one)s <> ALL("""+table+""".removed)
                AND ((sent = %(one)s AND took = %(two)s)
                    OR (sent = %(two)s AND took = %(one)s))""",
        {
            "one": login1,
            "two": login2
        }, **kwargs
    )

    delete = None
    # В диалогах можно сразу делать полноценную очистку
    if not is_conference:
        # Чтобы история переписки с самим собой не хранилась вечно
        if login1 == login2:
            length = 1
        else:
            length = 2

        delete = yield pool.execute(
            """DELETE FROM """+table+"""
                WHERE array_length("""+table+""".removed, 1) = %(length)s
                    AND ((sent = %(one)s AND took = %(two)s)
                        OR (sent = %(two)s AND took = %(one)s))""",
            {
                "one": login1,
                "two": login2,
                "length": length
            }, **kwargs
        )

    return (update, delete)
    # after use try/except for status operation

# Получает сообщение
def getMessage(mid, is_conference=False, **kwargs):
    pass


# Изменяет сообщение
def setMessage(mid, is_conference=False, **kwargs):
    pass


# Проверяет занятость адреса почты
def getSomeEMails(email, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    return pool.execute(
        "SELECT COUNT(*) FROM users WHERE email = %s LIMIT 1",
        [email], **kwargs
    )
    # after use .fetchone()[0]


# Проверяет занятость логина
def getSomeLogin(login, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    login = helpers.parse_login(login, True)[2][1:]

    return pool.execute(
        "SELECT COUNT(*) FROM users WHERE login = %s LIMIT 1",
        [login], **kwargs
    )
    # after use .fetchone()[0]


# Получает запрошенный инвайт
def getSomeInvite(invite, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    return pool.execute(
        "SELECT * FROM invites WHERE invite = %s LIMIT 1",
        [invite], **kwargs
    )
    # after use .fetchone()[0]


# Маркировка инвайта как использованного
def markInvite(invite, **kwargs):
    return pool.execute(
        "UPDATE invites SET active = false WHERE invite = %s",
        [invite], **kwargs
    )
    # after use try/except for status operation

# Проверяет занятость логина конференци
def checkConferenceExists(login, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    login = helpers.parse_login(login, True)[2][1:]

    return pool.execute(
        "SELECT COUNT(*) FROM conferences WHERE login = %s LIMIT 1",
        [login], **kwargs
    )
    # after use .fetchone()[0]

# Получает инфо о конференции
def getConference(login, **kwargs):
    login = helpers.parse_login(login, True)[2][1:]
    return pool.execute(
        "SELECT * FROM conferences WHERE login = %s LIMIT 1",
        [login], **kwargs
    )
    # after use .fetchone()

# добавляет конференцию
def addConference(conf, **kwargs):
    if "cursor_factory" not in kwargs.keys():
        kwargs["cursor_factory"] = psycopg2.extensions.cursor

    conf["login"] = helpers.parse_login(conf['login'], True)[2][1:]

    # Вычленяем только существующие в таблице ключи
    conf = {key: val for key, val in conf.items() if key in (
        "login",
        "name",
        "description",
        "members",
        "password",
        "mode"
    )}

    if 'members' in conf:
        conf['members'] = Json(conf['members'])

    return pool.execute(
        """
        INSERT
            INTO conferences(""" + ", ".join(conf.keys()) + """)
                VALUES (""" + ", ".join("%(" + key + ")s" for key in conf.keys()) + """)
        """,
        conf, **kwargs
    )
    # after use try/except for status operation

# Изменяет данные конференции
def setConference(data, search_by_login=None, **kwargs):

    # Вычленяем только существующие в таблице ключи
    conf = {key: val for key, val in data.items() if key in (
        "login",
        "name",
        "description",
        "members",
        "password",
        "mode"
    )}

    # Без изменения логина конференции.
    if search_by_login is None:
        conf["__search_by_login__"] = helpers.parse_login(conf.pop('login'), True)[2][1:]
    else:
        # Изменение логина с conf["__search_by_login__"] на conf['login']
        conf['login'] = helpers.parse_login(conf['login'], True)[2][1:]
        conf["__search_by_login__"] = helpers.parse_login(search_by_login, True)[2][1:]

    if 'members' in conf:
        conf['members'] = Json(conf['members'])

    return pool.execute(
        "UPDATE conferences SET "
        + ", ".join("%s = %s" % (key, "%("+key+")s") for key in conf.keys() if key != "__search_by_login__")
        + " WHERE login = %(__search_by_login__)s",
        conf, **kwargs
    )
    # after use try/except for status operation

def delConference(login, **kwargs):
    login = helpers.parse_login(login, True)[2][1:]

    return pool.execute(
        "DELETE FROM conferences WHERE login = %s",
        [login], **kwargs
    )
    # after use try/except for status operation


class ColumnCursor(psycopg2.extensions.cursor):
    """Class and class creator(set column key and parent cursor). Return only one column."""
    def __new__(cls, *args, column_key=None, parent_cursor=None, **kwargs):
        if column_key is None:
            obj = super(ColumnCursor, cls).__new__(cls)
            return obj
        else:
            # Перегрузка класса. Меняем родительский класс.
            if parent_cursor is not None:
                cls = type(cls.__name__, (parent_cursor,), dict(ColumnCursor.__dict__))

            cls.column_key = column_key
            return cls

    def __init__(self, *args, **kwargs):
        super(ColumnCursor, self).__init__(*args, **kwargs)

    def fetchone(self):
        return super(ColumnCursor, self).fetchone()[self.column_key]

    def fetchmany(self, size=None):
        return [x[self.column_key] for x in super(ColumnCursor, self).fetchmany(size)]

    def fetchall(self):
        return [x[self.column_key] for x in super(ColumnCursor, self).fetchall()]

    def __iter__(self):
        res = super(ColumnCursor, self).__iter__()
        first = next(res)[self.column_key]

        yield first
        while 1:
            yield next(res)[self.column_key]