#!/usr/bin/env python
"""
Testing modules.helpers
"""

import unittest
from modules import helpers, config

class HelpersTestCase(unittest.TestCase):
    """
    #helpers.recursive_merge()
    def test_recursive_merge(self):
        pass

    #helpers.filtering_conf_data()
    def test_filtering_conf_data(self):
        pass

    #helpers.filtering_user_data()
    def test_filtering_user_data(self):
        pass

    #helpers.is_exist_keys()
    def test_is_exist_keys(self):
        pass

    #helpers.is_request()
    def test_is_request(self):
        pass

    #helpers.unique()
    def test_unique(self):
        pass
    """

    #helpers.parse_skaro_login()
    def test_parse_skaro_login(self):
        test_login = "%s/@SomeName" % (config.read("host"),)
        self.assertEqual(
            helpers.parse_skaro_login(test_login),
            (config.read("host"), "@SomeName")
        )
        self.assertEqual(
            helpers.parse_skaro_login("@SomeName"),
            (config.read("host"), "@SomeName")
        )
        self.assertEqual(
            helpers.parse_skaro_login("SomeName"),
            (config.read("host"), "@SomeName")
        )

    #helpers.parse_login()
    def test_parse_login(self):
        # Default testing
        test_login = "%s/@SomeName" % (config.read("host"),)
        self.assertEqual(
            helpers.parse_login("SomeName"),
            ("skaro", test_login)
        )
        self.assertEqual(
            helpers.parse_login("skaro::SomeName"),
            ("skaro", "SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::@SomeName"),
            ("skaro", "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::"+test_login),
            ("skaro", test_login)
        )

        # testing with skaro login
        self.assertEqual(
            helpers.parse_login("SomeName", and_parse_skaro_login=True),
            ("skaro", config.read("host"), "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::SomeName", and_parse_skaro_login=True),
            ("skaro", config.read("host"), "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::@SomeName", and_parse_skaro_login=True),
            ("skaro", config.read("host"), "@SomeName")
        )
        self.assertEqual(
            helpers.parse_login("skaro::"+test_login, and_parse_skaro_login=True),
            ("skaro", config.read("host"), "@SomeName")
        )

    #helpers.recover_full_login()
    def test_recover_full_login(self):
        test_login = "skaro::%s/@SomeName" % (config.read("host"),)
        self.assertEqual(
            helpers.recover_full_login("skaro::%s/@SomeName" % (config.read("host"),)),
            test_login
        )
        self.assertEqual(
            helpers.recover_full_login("%s/@SomeName" % (config.read("host"),)),
            test_login
        )
        self.assertEqual(
            helpers.recover_full_login("@SomeName"),
            test_login
        )
        self.assertEqual(
            helpers.recover_full_login("SomeName"),
            test_login
        )


if __name__ == '__main__':
    unittest.main()
