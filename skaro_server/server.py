#!/usr/bin/env python

import sys
import logging
import copy
from random import randint
# for crypting
import uuid
import hmac
import hashlib
# for timing
from datetime import datetime, timedelta

# Tornado server
import tornado.escape
import tornado.util
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.websocket
from tornado.options import define, options, parse_command_line

# No standard python libraries
import cbor

# Local modules and packages
import protocols
from modules import config, database, helpers, validate as VL
from modules.validate import DictSchema, IntValue, NumericValue, BoolValue, StringValue, ListValue, DictValue

logger = logging.getLogger(__name__)  # named logging for server.py
define("port", default=8000, type=int, help="run on the given port")


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/skaro_client/", WSHandlerClient),
            (r"/skaro_server/", WSHandlerServer),
        ]
        settings = dict(
            xsrf_cookies=False,
            autoload=True
        )

        # Подключаемся к бд
        database.connect()

        # Множество подключенных в данный момент пользователей(не авторизованных)
        # Каждый пользователь, это экземпляр класса WSHandler
        # todo: организовать очистку от соединений, которые не авторизовались в течение 10ти минут.
        # todo: с выводом уведомления о причине разрыва соединения
        self.SocketsPool = set()

        # Словарь соединений авторизованных пользователей
        # перекладываются сюда из self.SocketsPool после авторизации
        # {
        #     "@SomeName": {
        #         "connections": {
        #              "__идентификатор устройства__": object,
        #              "__идентификатор устройства__": object,
        #              ...
        #         },
        #         "data": {USER DATA},
        #         "accounts": {PROTOCOLS HANDLERS}
        #     },
        #     ...
        # }
        self.AuthorizedPool = dict()

        # Сессии пользователей, потерявших соединение
        self.SessionsRestore = list()

        tornado.web.Application.__init__(self, handlers, **settings)

    # Чистильщик, работающий асинхронно
    # Следит за тем, чтобы прерванные сессии не лежали в хранилище слишком долго
    def cleaner_sessions_restore(self):
        # Осуществляем проход по хранилищу сессий
        for session in self.SessionsRestore:
            if session['__time_of_dead__'] < datetime.now():
                self.SessionsRestore.remove(session)
        # Запускам чистильщик снова, если есть что чистить
        if len(self.SessionsRestore) > 0:
            tornado.ioloop.IOLoop.instance().call_later(5, self.cleaner_sessions_restore)

    #Добавляет потерянную сессию в хранилище быстрого восстановления сессии
    def add_session_in_store(self, session_id=None, device_id=None, login=None, lifetime=5):
        # Узнаем, остановлен ли чистильщик сессий
        # Если хранилище сессий пустое, то чистильщик сессий остановлен
        start_cleaner = False
        if len(self.SessionsRestore) < 1:
            start_cleaner = True

        # Временно сохраняем сессию в хранилище
        self.SessionsRestore.append({
            "sid": session_id,
            "login": helpers.recover_full_login(login),
            "device_id": device_id,
            "__time_of_dead__": datetime.now() + timedelta(minutes=lifetime),
        })

        # Запускаем чистильщик сессий, если он был остановлен
        if start_cleaner:
            tornado.ioloop.IOLoop.instance().call_later(lifetime, self.cleaner_sessions_restore)


class WSHandler(tornado.websocket.WebSocketHandler):
    # Проверка подключения
    def check_origin(self, origin):
        logger.info(origin)
        return True

    # Если соединение открывается с указанием подпротокола
    # todo: необходимо придумать систему поддержки не только CBOR, но и JSON и т.п.
    def select_subprotocol(self, subprotocols):
        if "skaro" in subprotocols:
            return "skaro"
        else:
            return None

    # Декодируем сообщение от клиента
    def read_client(self, message):
        try:
            data = cbor.loads(message)
        except:
            logger.error("Error decoding message", exc_info=True)
            return False
        else:
            return data

    # Отправляем сообщение клиенту
    def write_client(self, message):
        logger.info(message)
        try:
            message = cbor.dumps(message)
            self.ws_connection.write_message(message, binary=True)
        except:
            logger.error("Error sending message", exc_info=True)

    # Отправка сообщения с ошибкой или успехом операции
    def write_info(self, data, status="400 Bad Request", info="Some error"):
        if helpers.is_request(data):
            self.write_client({
                "type": "request",
                "for": data["type"],
                "id": data["on_request"],
                "status": status,
                "info": info,
            })
        else:
            self.write_client({
                "type": "error",
                "status": status,
                "info": info,
            })

    # Генерация уникального токена
    def generate_token(self):
        # Секрет динамический, зависит от обработчика ws соединения
        key = bytes(hex(hash(self) + id(self)), 'utf-8')
        # Кодированное сообщение зависит от текущего времени и рандомной приставки
        msg = bytes(str(datetime.today()) + str(uuid.uuid4()), 'utf-8')
        # HMAC-SHA224
        return hmac.new(key=key, msg=msg, digestmod=hashlib.sha224).hexdigest()




class WSHandlerClient(WSHandler):

    session = None
    device_id = None
    uid = None

    def is_auth(self):
        return self.session is not None and hasattr(self, "sid")

    # Первое подключение, открытие ws соединения
    def open(self):
        self.uid = id(self)
        self.device_id = helpers.generate_device_id(self)

        logger.info("Кто-то открыл соединение: " + self.device_id + "(" + str(self.uid) + ")")

        # Проверяем соединение
        self.token = self.generate_token()
        self.write_client({
            "type": "check_connection",
            "token": self.token,
        })

    # Соединение прервалось
    def on_close(self):

        if self.is_auth():
            logger.info("Пользователь потерял соединение: " + self.session.data['login'] + "(" + str(self.device_id) + ")")
        else:
            logger.info("Кто-то потерял соединение: " + self.device_id + "(" + str(self.uid) + ")")

            # заканчиваем операции для не авторизованных
            del self
            return


        # Удаляем из онлайна
        del self.session.connections[self.device_id]
        if not len(self.session.connections):
            self.session.data["status"] = "offline"
            self.send_updates({
                "type": "changed_user_data",
                "login": self.session.data['login'],
                "update": {
                    "status":"offline"
                }
            })

            del self.application.AuthorizedPool[
                helpers.parse_skaro_login(self.session.data['login'])[1]
            ]

        # Добавляем сесссию потерянного соединения в хранилище быстрого восстановления сессий
        self.application.add_session_in_store(
            session_id=self.sid,
            device_id=self.device_id,
            login=self.session.data['login']
        )

        del self

    # Перехватываем сообщение
    def on_message(self, message):
        if self.is_auth():
            logger.info("От пользователя " + self.session.data['login'] + "(" + str(self.device_id) + ")")
        else:
            logger.info("От неавторизованного пользователя " + self.device_id + "(" + str(self.uid) + ")")

        # Декодируем данные
        data = self.read_client(message)
        logger.info("Получено сообщение %r", data)

        # Проверка формата данных
        if not data:
            self.write_client({
                "type": "error",
                "status": "400 Bad Request",
                "info": "Неверный формат, не удалось декодировать сообщение",
            })

            return
        elif type(data) is not dict:
            self.write_client({
                "type": "error",
                "status": "400 Bad Request",
                "info": "Неверный формат сообщения: нарушена структура \"ключ-значение\"",
            })

            return
        elif "type" not in data.keys() or type(data['type']) is not str:
            if helpers.is_request(data):
                self.write_client({
                    "type": "request",
                    "for": "unknown",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Неверный формат сообщения: отсутствует поле \"type\"",
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Неверный формат сообщения: отсутствует поле \"type\"",
                })

            return

        # Запросы, на которые сервер должен отвечать всегда
        # Запрос на авторизацию, тип регистрации, регистрацию, восстановление сессии подключения
        if data["type"] in ("authorize", "register_type", "register", "session_restore"):
            getattr(self, "event_" + data["type"])(data)
            return

        # Остальные виды запросов только после авторизации
        # Если гость, то выводим соответствующее сообщение
        elif not self.is_auth():
            if helpers.is_request(data):
                self.write_client({
                    "type": "request",
                    "for": data["type"],
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Данный тип сообщений поддерживается только после авторизации или не поддерживается вообще.",
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Данный тип сообщений (%s) поддерживается только после авторизации или не поддерживается вообще."
                            % (data["type"],),
                })

            return

        # Вызов обработчиков остальных сообщений
        try:
            getattr(self, "event_" + data["type"])(data)
        except AttributeError:
            if helpers.is_request(data):
                self.write_client({
                    "type": "request",
                    "for": data["type"],
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Данный тип сообщений не поддерживается.",
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Данный тип сообщений ('" + data["type"] + "') не поддерживается.",
                })

    # Данные пользователя изменились
    @tornado.web.asynchronous
    def sysevent_changed_user_data(self, data):
        try:
            # Пришло уведомление об изменении логина
            if "login" in data["update"].keys():
                # Пользователь ИЗ КОНТАКТОВ изменил свой логин
                if data["login"] != self.session.data["login"]:
                    # Меняем данные если юзер с таким логином действительно есть в контактах
                    if data["login"] in self.session.data["contacts"]:
                        # Кроме того, мы уже могли поменять логин.
                        if data["update"]["login"] not in self.session.data["contacts"]:
                            # и, наконец, меняем логин контакта
                            self.session.data["contacts"][data["update"]["login"]]\
                                = self.session.data["contacts"][data["login"]]

                            del self.session.data["contacts"][data["login"]]

        except:
            logger.error("Error handling data: needed key is not exists", exc_info=True)
            return

        self.write_client(data)

    # Изменился список участников конференции(в которой self юзер уже является участником)
    def sysevent_changed_conference_members(self, data):
        self.write_client(data)

    # Изменились данные конферении
    def sysevent_changed_conference(self, data):
        self.write_client(data)

    # Изменился ваш список контактов
    def sysevent_changed_contacts(self, data):
        self.write_client(data)

    # Отправляет что-то всем у кого юзер в друзьях
    @tornado.web.asynchronous
    @tornado.gen.engine
    def send_updates(self, data, users_for_update=False, for_conference=False, send_self_devices=False):
        if not type(data) is dict or "type" not in data.keys():
            logger.error("Error handling data: invalid format")
            return

        logger.info("sending message to all contacts")

        if not users_for_update:
            # Загружаем список тех, кого уведомлять
            try:
                if for_conference:
                    cursor = yield database.getAtWhomInFavorites(for_conference, is_conference=True)
                else:
                    cursor = yield database.getAtWhomInFavorites(self.session.data["login"])
            except:
                logger.error("Error getting users to send notifications", exc_info=True)
                return
            else:
                users_for_update = cursor.fetchall()

        # Удаление повторений
        users_for_update = set(users_for_update)

        # Если нужно, то добавляем свой логин.
        # Тогда сообщение отправится на все устройства пользователя, кроме текущего устройства.
        if send_self_devices:
            users_for_update.add(self.session.data["login"])

        # Уведомляем
        for us_login in users_for_update:
            us_login = helpers.recover_full_login(us_login)
            us_data = self.application.AuthorizedPool.get(
                helpers.parse_skaro_login(us_login)[1]
            )

            if us_data is None:
                #todo: если юзер не в онлайне ему тоже положено знать о некоторых событиях.
                #todo: Например в сообщении на почту. Но это должно быть настраиваемо пользователем.
                continue

            for waiter in us_data.connections.values():
                if waiter.uid == self.uid: continue

                logger.info("Пользователю: %s (%s)", waiter.session.data['login'], waiter.device_id)

                if hasattr(waiter, "sysevent_"+data["type"]):
                    # С предварительной обработкой
                    getattr(waiter, "sysevent_"+data["type"])(data)
                else:
                    logger.error("Пользователю %s (%s) не удалось отправить сообщение.\
                                  Не найден обработчик сообщения этого типа",
                                 waiter.session.data['login'], waiter.device_id)

    # Возвращает данные о юзере
    # вызов только через user_data = yield self.get_user_data(login)
    @tornado.gen.coroutine
    def get_user_data(self, full_login):
        user = False
        (protocol, login_in_prl) = helpers.parse_login(full_login)
        if protocol == "skaro":
            (host, login) = helpers.parse_skaro_login(login_in_prl)

            # TODO: Если контакт - конференция
            if login[0] == "~":
                return False  # Аналогично raise tornado.gen.Return(False)

            if host == config.read("host"):
                # Ищем юзера в онлайне
                user = {"status": "offline"}
                us_data = self.application.AuthorizedPool.get(
                    helpers.parse_skaro_login(full_login)[1]
                )
                if us_data is not None:
                    user = helpers.filtering_user_data(us_data.data.copy())

                if user["status"] == "offline":
                    try:
                        cursor = yield database.getUserByLogin(login[1:])
                    except:
                        logger.error("Error in get user data", exc_info=True)
                        user = False
                    else:
                        user = cursor.fetchone()
                        if user:
                            user = helpers.filtering_user_data(dict(user))
                            user['login'] = helpers.recover_full_login(user['login'])
                            user["status"] = "offline"
                        else:
                            user = False
            else:
                # Обращение к другому skaro серверу...
                pass
        else:
            user = self.session.accounts[protocol].get_user_data(login_in_prl)
            if user:
                user = helpers.filtering_user_data(user)

        return user  # Аналогично raise tornado.gen.Return(user)

    # Возвращает данные о юзере
    # вызов только через user_data = yield self.get_conf_data(login)
    @tornado.gen.coroutine
    def get_conf_data(self, full_login):
        conf = False
        (protocol, login_in_prl) = helpers.parse_login(full_login)
        if protocol == "skaro":
            (host, login) = helpers.parse_skaro_login(login_in_prl)

            # TODO: Если контакт - не конференция
            if login[0] == "@":
                return False  # Аналогично raise tornado.gen.Return(False)

            if host == config.read("host"):
                try:
                    cursor = yield database.getConference(login[1:])
                except:
                    logger.error("Error in get conference data", exc_info=True)
                    conf = False
                else:
                    conf = cursor.fetchone()
                    if conf:
                        conf = helpers.filtering_conf_data(dict(conf))
                        conf['login'] = helpers.recover_full_login(conf['login'], with_symbol="~")
                    else:
                        conf = False
            else:
                # Обращение к другому skaro серверу...
                pass
        else:
            conf = self.session.accounts[protocol].get_conf_data(login_in_prl)
            if conf:
                conf = helpers.filtering_conf_data(conf)

        return conf  # Аналогично raise tornado.gen.Return(user)

    # Получает полную информацию о контактах пользователя
    # вызов только через yield self.get_self_contacts()
    @tornado.gen.coroutine
    def get_self_contacts(self):
        contacts = []
        # Прогружаем данные контактов (тут только skaro протокол)
        for login, settings in self.session.data['contacts'].items():
            (protocol, login_in_prl) = helpers.parse_login(login)

            if protocol != "skaro":
                continue
            if settings.get("type") != "conference":
                user = yield self.get_user_data(login)
                if user:
                    contacts.append(user)
                else:
                    # Информируем пользователя
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке получить информацию о контактах пользователя"
                    })
            else:
                conf = yield self.get_conf_data(login)
                if conf:
                    contacts.append(conf)
                else:
                    # Информируем пользователя
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке получить информацию о контактах пользователя"
                    })

        # Добавляем контактов из доп. протоколов.
        for protocol, cls in self.session.accounts.items():
            if protocol in protocols.__all__:
                # Предварительно очищаем от контактов этого протокола старый список контактов
                contacts = [cn for cn in contacts if helpers.parse_login(cn["login"])[0] != protocol]
                # а уже потом добавляем
                try:
                    contacts += cls.get_contacts()
                except:
                    # Отправляем клиенту, что произошла ошибка
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке \"%s\" получить информацию о контактах пользователя" % (protocol,),
                    })

        return contacts

    # Дополнительно заполняет данными обьект соединения с клиентом
    # вызов только через yield self.init_self_user()
    # прогружает список контактов(без полной о них информации), авторизуется в прикрепленных аккаунтах
    @tornado.gen.coroutine
    def init_self_user(self):

        # Получаем логины контактов, с которыми есть открытые диалоги
        try:
            cursor = yield database.getChats(self.session.data['login'])
        except:
            contacts = self.session.data['contacts']
            logger.error("Error get contacts", exc_info=True)
            # Информируем пользователя
            self.write_client({
                "type": "error",
                "status": "500 Internal Server Error",
                "info": "Ошибка при попытке получить контакты пользователя",
            })
        else:
            # Добавляем их в контакт-лист, если их еще там нет
            contacts = {login: {} for login in cursor.fetchall()}
            contacts.update(self.session.data['contacts'])


        # Авторизуемся в прикрепленных протоколах и сохраняем интерфейс работы с ними.
        # Добавляем контактов из доп. протоколов.
        for login, settings in self.session.data['accounts'].items():
            (protocol, login_in_prl) = helpers.parse_login(login)
            if protocol in protocols.__all__:
                try:
                    mod = tornado.util.import_object('protocols.' + protocol)
                    cls = mod.common(self, settings)
                    if cls:
                        # Сохраняем интерфейс
                        self.session.accounts[protocol] = cls

                        # Предварительно очищаем от контактов этого протокола
                        contacts = {login: settings
                                        for login, settings in contacts.items()
                                            if helpers.parse_login(login)[0] != protocol}
                        # а уже потом добавляем
                        contacts.update(cls.get_contacts_settings())
                    else:
                        raise ImportError('Unable to create an instance of the handler in the module "%s"'
                                          % (protocol,))
                except:
                    logger.error('Module %s not imported.' % (protocol), exc_info=True)
                    # Отправляем клиенту, что произошла ошибка
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Ошибка авторизации в %s" % (protocol),
                    })

        # Обновление списка контактов, если нужно
        if contacts != self.session.data['contacts']:
            self.session.data['contacts'] = contacts
            try:
                yield database.setUser({"contacts": self.session.data['contacts'], "login": self.session.data['login']})
            except:
                logger.error("Error update contacts", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке обновить список контактов пользователя",
                })

    # Осуществляет отправку сообщения
    @tornado.gen.coroutine
    def sys_send_message(self, data, hidden_for=None):
        mid, status, info = (None, '200 OK', [])
        date = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))

        # Формируем данные для отправки сообщения
        mess = {
            "type": "show_message",
            "date": date,
            "from": {
                "name": self.session.data['name'],
                "login": self.session.data['login'],
            },
            "body": data["body"]
        }

        data["date"] = date
        data["from"] = self.session.data['login']


        # Добавляем сообщение в историю
        try:
            cursor = yield database.addHistory(data, hidden_for)
        except:
            status = "250 Done with errors"
            info.append("Не удалось записать в историю")
            logger.error("Error adding history", exc_info=True)
        else:
            mid = mess["mid"] = cursor.fetchone()[0]

        # Отправляем сообщение
        (protocol, login_in_prl) = helpers.parse_login(data['to'])

        # Стандартный протокол skaro
        if protocol == 'skaro':

            us_data = self.application.AuthorizedPool.get(
                helpers.parse_skaro_login(data["to"])[1]
            )

            if us_data is not None:
                # Добавляем себя в контакты адресату, если вы еще не являетесь его контактом
                if self.session.data["login"] not in us_data.data["contacts"]:
                    us_data.data["contacts"][self.session.data["login"]] = {}
                    try:
                        yield database.setUser({
                            "contacts": us_data.data["contacts"],
                            "login": us_data.data["login"]
                        })
                    except:
                        status = "250 Done with errors"
                        info.append("Не удалось обновить список контактов ответчика (%s)" % (us_data.data["login"],))
                        logger.error("Error updating contact list (db)", exc_info=True)

                for waiter in us_data.connections.values():
                    waiter.write_client(mess)

            # Отправляем свое сообщение на собственные устройства, которые тоже подключены
            mess["to"] = helpers.recover_full_login(data['to'])
            for device_id, waiter in self.session.connections.items():
                if device_id != self.device_id:
                    waiter.write_client(mess)

        # Иной протокол
        else:
            if protocol not in self.session.accounts.keys():
                status = "403 Forbidden"
                info.append("Не удалось отправить сообщение. Вам необходимо иметь аккаунт \"%s\"." % (protocol,))
            else:
                cls = self.session.accounts[protocol]
                try:
                    if cls.send_message(data["to"], data["body"]) is False:
                        raise Exception('send_message method returned False')
                except:
                    status = "500 Internal Server Error"
                    info.append("Не удалось отправить сообщение. Ошибка в модуле обработчике протокола.")
                    logger.error("Error sending message", exc_info=True)

            if status[:3] == "200":
                pass

        # Если ответчик не в контактах, то добавляем его в контакты
        if data["to"] not in self.session.data["contacts"].keys():

            self.session.data["contacts"].update({data["to"]: {}})
            try:
                yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
            except:
                logger.error("Error update contacts", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке обновить список контактов пользователя",
                })

        return (mid, status, info)


    # Осуществляет отправку сообщения
    @tornado.gen.coroutine
    def sys_send_conf_message(self, data, hidden_for=None):
        mid, status, info = (None, '200 OK', [])
        date = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))

        conf_login = helpers.recover_full_login(data["to"], with_symbol="~")

        # Формируем данные для отправки сообщения
        mess = {
            "type": "show_message",
            "date": date,
            "from": {
                "name": self.session.data['name'],
                "login": self.session.data['login'],
                "in_conference": conf_login
            },
            "body": data["body"]
        }

        data["date"] = date
        data["from"] = self.session.data['login']

        # Отправляем сообщение
        (protocol, login_in_prl) = helpers.parse_login(conf_login, to_recover_with="~")
        # Стандартный протокол skaro
        if protocol == 'skaro':

            # Проверка на существование конфы
            try:
                cursor = yield database.checkConferenceExists(conf_login)
            except:
                status = "500 Internal Server Error"
                info.append("Не удалось проверить существование такой конференции")
                logger.error("Error checking conference login", exc_info=True)
            else:
                if not cursor.fetchone()[0]:
                    status = "404 Bad request"
                    info.append("Такой конференции не существует")
                else:
                    # Добавляем сообщение в историю
                    try:
                        cursor = yield database.addHistory(data, is_conference=True)
                    except:
                        status = "250 Done with errors"
                        info.append("Не удалось записать в историю")
                        logger.error("Error adding history", exc_info=True)
                    else:
                        mid = mess["mid"] = cursor.fetchone()[0]

                    # Получение списка участников конференции
                    conf_data = None
                    try:
                        cursor = yield database.getConference(conf_login)
                    except:
                        status = "500 Internal Server Error"
                        info.append("Не удалось получить список участников конференции")
                        logger.error("Error getting conference members", exc_info=True)
                    else:
                        conf_data = cursor.fetchone()

                    # Проверки прав доступа
                    is_send = False
                    if conf_data:
                        is_send = True

                        # Проверка на существование отправителя в списке участников конференции
                        my_member_data = conf_data["members"].get(self.session.data["login"], None)
                        if my_member_data is None:
                            status = "405 Not Allowed"
                            info.append("Вы не являетесь участником конференции")
                            is_send = False

                        # Проверка, что отправитель не Гость. Гостям запрещено отправлять сообщения
                        if is_send:
                            my_group = my_member_data.get("group", "quest")
                            if my_group == "quest":
                                status = "405 Not Allowed"
                                info.append("Группе \"%s\" запрещено отправлять сообщения в конференцию".format(my_group))
                                is_send = False

                    # Отправка
                    if is_send:
                        # поиск участников конференции онлайн
                        for member_login, member_info in conf_data["members"].items():
                            us_data = self.application.AuthorizedPool.get(
                                helpers.parse_skaro_login(member_login)[1]
                            )

                            if us_data is not None:
                                # Добавляем конференцию в контакты,
                                # если у участника конференции она еще не в контакатах
                                if conf_login not in us_data.data["contacts"]:
                                    us_data.data["contacts"][conf_login] = {"type":"conference"}
                                    try:
                                        yield database.setUser({
                                            "contacts": us_data.data["contacts"],
                                            "login": us_data.data["login"]
                                        })
                                    except:
                                        status = "250 Done with errors"
                                        info.append("Не удалось обновить список контактов ответчика (%s)" % (us_data.data["login"],))
                                        logger.error("Error updating contact list (db)", exc_info=True)

                                for waiter in us_data.connections.values():
                                    # непоср. отправка
                                    waiter.write_client(mess)

                            # Участник не в онлайне
                            else:
                                # Делаем что-то, или не делаем...
                                pass

                        # Отправляем свое сообщение на собственные устройства, которые тоже подключены
                        mess["to"] = conf_login
                        for device_id, waiter in self.session.connections.items():
                            if device_id != self.device_id:
                                waiter.write_client(mess)


        # Иной протокол
        else:
            if protocol not in self.session.accounts.keys():
                status = "403 Forbidden"
                info.append("Не удалось отправить сообщение. Вам необходимо иметь аккаунт \"%s\"." % (protocol,))
            else:
                cls = self.session.accounts[protocol]
                try:
                    if cls.send_conf_message(data["to"], data["body"]) is False:
                        raise Exception('send_message method returned False')
                except:
                    status = "500 Internal Server Error"
                    info.append("Не удалось отправить сообщение. Ошибка в модуле обработчике протокола.")
                    logger.error("Error sending message", exc_info=True)

            if status[:3] == "200":
                # Добавляем сообщение в историю
                try:
                    cursor = yield database.addHistory(data, is_conference=True)
                except:
                    status = "250 Done with errors"
                    info.append("Не удалось записать в историю")
                    logger.error("Error adding history", exc_info=True)
                else:
                    mid = mess["mid"] = cursor.fetchone()[0]


        # Если ответчик не в контактах, то добавляем его в контакты
        if data["to"] not in self.session.data["contacts"].keys():

            self.session.data["contacts"].update({data["to"]: {"type":"conference"}})
            try:
                yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
            except:
                logger.error("Error update contacts", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке обновить список контактов пользователя",
                })

        return (mid, status, info)


    # Удаляет историю
    @tornado.gen.coroutine
    def clear_history(self, login):
        try:
            yield database.clearHistory(self.session.data["login"], login)
        except:
            logger.error("Error database.clearHistory(%s, %s)", self.session.data["login"], login, exc_info=True)
            return False
        else:
            return True

    # Выход пользователя
    def event_logout(self, data: DictSchema(
            type = StringValue(required=True),
            not_close_connection = BoolValue(),
            on_request = IntValue()
        )):
        if "not_close_connection" not in data.keys():
            data["not_close_connection"] = False

        logger.info("Пользователь вышел: " + self.session.data['login'] + "(" + str(self.device_id) + ")")

        # Удаляем из онлайна
        del self.session.connections[self.device_id]
        if not len(self.session.connections):
            self.session.data["status"] = "offline"
            self.send_updates({
                "type": "changed_user_data",
                "login": self.session.data['login'],
                "update": {
                    "status":"offline"
                }
            })
            del self.application.AuthorizedPool[helpers.parse_skaro_login(self.session.data['login'])[1]]

        # Добпавляем сесссию потерянного соединения в хранилище быстрого восстановления сессий
        self.application.add_session_in_store(
            session_id=self.sid,
            login=self.session.data['login']
        )

        # заканчиваем операции для не авторизованных
        if data["not_close_connection"] is False:
            del self

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_register(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
            invite = StringValue(max=255),
            login = StringValue(
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            email = StringValue(required=True, max=255),
            password = StringValue(required=True, max=255)
        )):

        if config.read('register_type') == 'invite':
            if not helpers.is_exist_keys(data, 'invite'):
                self.write_client({
                    "type": "request",
                    "for": "register",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Для регистрации требуется приглашение"
                })
                return
            else:
                try:
                    cursor = yield database.getSomeInvite(data['invite'])
                except:
                    logger.error("Error get invites", exc_info=True)
                    # Информируем пользователя
                    self.write_client({
                        "type": "request",
                        "for": "register",
                        "id": data["on_request"],
                        "status": "500 Internal Server Error",
                        "info": "Ошибка при попытке проверки существования приглашения",
                    })
                    return
                else:
                    invite = cursor.fetchone()
                    if invite is None:
                        self.write_client({
                            "type": "request",
                            "for": "register",
                            "id": data["on_request"],
                            "status": "400 Bad Request",
                            "info": "Приглашение не найдено"
                        })
                        return
                    elif not invite[2]:
                        self.write_client({
                            "type": "request",
                            "for": "register",
                            "id": data["on_request"],
                            "status": "400 Bad Request",
                            "info": "Приглашение не действительно"
                        })
                        return

        # Элементарная проверка адреса почты, для предотвращения лишних действий
        if not VL.is_valid(data['email'], VL.P_MAIL):
            self.write_client({
                "type": "request",
                "for": "register",
                "id": data["on_request"],
                "status": "400 Bad Request",
                "info": "Недопустимый адрес почты"
            })
            return

        # Элементарная проверка логина, для предотвращения лишних действий
        if helpers.is_exist_keys(data, 'login') and not VL.is_valid(data['login'], VL.P_LOGIN):
            self.write_client({
                "type": "request",
                "for": "register",
                "id": data["on_request"],
                "status": "400 Bad Request",
                "info": "Недопустимый логин"
            })
            return

        try:
            cursor = yield database.getSomeEMails(data['email'])
        except:
            logger.error("Error get count", exc_info=True)
            # Информируем пользователя
            self.write_client({
                "type": "request",
                "for": "register",
                "id": data["on_request"],
                "status": "500 Internal Server Error",
                "info": "Ошибка при попытке проверки занятости адреса почты",
            })
            return
        else:
            if cursor.fetchone()[0] > 0:
                self.write_client({
                    "type": "request",
                    "for": "register",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Этот адрес почты уже занят"
                })
                return

        # Генерация временного логина, который пользователь сможет изменить в настройках
        if not helpers.is_exist_keys(data, 'login'):
            data['login'] = 'u' + str(randint(100000, 999999))

        try:
            cursor = yield database.getSomeLogin(data['login'])
        except:
            logger.error("Error get count", exc_info=True)
            # Информируем пользователя
            self.write_client({
                "type": "request",
                "for": "register",
                "id": data["on_request"],
                "status": "500 Internal Server Error",
                "info": "Ошибка при попытке проверки занятости логина, попробуйте еще раз",
            })
            return
        else:
            if cursor.fetchone()[0] > 0:
                self.write_client({
                    "type": "request",
                    "for": "register",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Этот логин уже занят"
                })
                return

        try:
            yield database.addUser(data)
        except:
            logger.error("Error adding user", exc_info=True)
            self.write_client({
                "type": "request",
                "for": "register",
                "id": data["on_request"],
                "status": "400 Bad Request",
                "info": "Не удалось зарегистрироваться"
            })
            return

        if config.read('register_type') == 'invite':
            try:
                yield database.markInvite(data['invite'])
            except:
                logger.error("Error marking invite", exc_info=True)
                self.write_client({
                    "type": "request",
                    "for": "register",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Не удалось зарегистрироваться"
                })
                return

        # Операция прошла успешно
        self.write_client({
            "type": "request",
            "for": "register",
            "id": data["on_request"],
            "status": "200 OK",
            "login": data['login'],
        })

    @tornado.web.asynchronous
    @VL.validate_args
    def event_register_type(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
        )):

        register_type = config.read('register_type')
        if register_type not in ['open', 'invite', 'close']:
            register_type = 'open'

        self.write_client({
            "type": "request",
            "for": "register_type",
            "id": data["on_request"],
            "status": "200 OK",
            "register_type": register_type,
        })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_authorize(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            device_id = StringValue(max=64, pattern=VL.P_DEVICE_ID),
            password = StringValue(required=True, max=255)
        )):

        # Элементарная проверка, для предотвращения лишних действий
        if VL.is_valid(data['login'], VL.P_MAIL):
            is_mail = 1
        elif VL.is_valid(data['login'], VL.P_LOGIN):
            is_mail = 0
        else:
            if helpers.is_request(data):
                self.write_client({
                    "type": "request",
                    "for": "authorize",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Недопустимый логин"
                })
            return

        # Получение данных юзера по логину или email.
        # По идее, это не всегда нужно делать с обращением к БД.
        # Т.к. если пользователь входит в свой аккаунт одновременно с другим устройством,
        # эти данные уже получены и хранятся в оперативной памяти сервера.
        # Так что это пусть не идельное, но зато локаничное решение.
        # На общую производительность оно практически не влияет.
        try:
            if is_mail:
                cursor = yield database.getUserByEMail(data['login'])
            else:
                cursor = yield database.getUserByLogin(data['login'])
        except:
            logger.error("Error get user data", exc_info=True)
            user = None
        else:
            user = cursor.fetchone()

        if user:
            # Если в качестве логина пользователь ввел email
            data['login'] = user["login"]

            # Генерируем HMAC-SHA512 от токен+пароль так же как клиент
            hash = hmac.new(key=bytes(user["password"], 'utf-8'), msg=bytes(self.token, 'utf-8'),
                            digestmod=hashlib.sha512)
            # Сверяем хеши(должны быть одинаковы при правильном пароле)
            if hash.hexdigest() == data['password']:
                status = '200 OK'
                info = ''
                user['login'] = helpers.recover_full_login(user['login'])

                # Меняем device_id, если он задан.
                if "device_id" in data.keys():
                    self.device_id = data["device_id"]

                # Получаем "голенький" логин, без протокола и хоста.
                login = helpers.parse_skaro_login(user['login'])[1]

                # Проверка на то, что название устройства не занято
                if login not in self.application.AuthorizedPool.keys() \
                or self.device_id not in self.application.AuthorizedPool[login].connections.keys():

                    # Обновляем словарь авторизованных пользователей,
                    # если с других аккаунтов пользователя это еще не было сделано
                    self.application.AuthorizedPool.setdefault(
                        login,
                        type("AuthorizedClient", (object,), {
                            "connections": {},
                            "data": user,
                            "accounts": {}
                        })
                    )

                    # Сохраняем ссылку на ячейку в словаре,
                    # чтобы не искать каждый раз самого себя.
                    self.session = self.application.AuthorizedPool[login]

                    # Прогрузка данных пользователя (self.session)
                    yield self.init_self_user()

                    # Генерируем идентификатор сессии пользователя
                    self.sid = hmac.new(key=bytes(data["password"], 'utf-8'), msg=bytes(self.token, 'utf-8'),
                                        digestmod=hashlib.sha224).hexdigest()

                    # Удаляем токен, он больше нам не нужен.
                    del self.token

                    # Добавляем авторизованное устройство в множество авторизованных устройств
                    self.session.connections[self.device_id] = self
                    # Если другие устройства небыли онлайн "до нас", то уведомляем всех о нашем входе.
                    if len(self.session.connections) == 1:
                        self.session.data["status"] = "online"
                        self.send_updates({
                            "type": "changed_user_data",
                            "login": self.session.data['login'],
                            "update": {
                                "status":"online"
                            }
                        })
                else:
                    status = '400 Bad Request'
                    info = 'Название устройства "%s" уже занято.' % (self.device_id,)
            else:
                status = '400 Bad Request'
                info = "Неправильный логин/пароль."
        else:
            status = '400 Bad Request'
            info = "Ошибка при попытке поиска пользователя с таким именем."

        if helpers.is_request(data):
            req = {
                "type": "request",
                "for": "authorize",
                "id": data["on_request"],
                "status": status,
            }
            if status[0:3] == '200':
                req['session_id'] = self.sid
                req['device_id'] = self.device_id
                req['user'] = helpers.filtering_user_data(copy.deepcopy(self.session.data))

            if info:
                req["info"] = info

            self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_session_restore(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
            sid = StringValue(required=True, max=255),
            device_id = StringValue(max=64, pattern=VL.P_DEVICE_ID)
        )):

        # Получаем данные юзера из его сессии(если она еще доступна)
        res_session = None
        for s in self.application.SessionsRestore:
            if s['sid'] == data['sid']:
                res_session = s.copy()
                self.application.SessionsRestore.remove(s)
                break

        if res_session is not None:
            status = '200 OK'
            info = ''
            (host, slogin) = helpers.parse_skaro_login(helpers.parse_login(res_session['login'])[1])
            try:
                cursor = yield database.getUserByLogin(slogin[1:])
            except:
                logger.error("Error get user data", exc_info=True)
                user = None
            else:
                user = cursor.fetchone()

            if user:
                user['login'] = helpers.recover_full_login(user['login'])

                # Вспоминаем device_id
                if res_session["device_id"] is not None:
                    self.device_id = res_session["device_id"]

                # Получаем "голенький" логин, без протокола и хоста.
                login = helpers.parse_skaro_login(user['login'])[1]

                # Проверка на то, что название устройства не занято
                if login not in self.application.AuthorizedPool.keys() \
                or self.device_id not in self.application.AuthorizedPool[login].connections.keys():

                    # Обновляем словарь авторизованных пользователей,
                    # если с других аккаунтов пользователя это еще не было сделано
                    self.application.AuthorizedPool.setdefault(
                        login,
                        type("AuthorizedClient", (object,), {
                            "connections": {},
                            "data": user,
                            "accounts": {}
                        })
                    )

                    # Сохраняем ссылку на ячейку в словаре,
                    # чтобы не искать каждый раз самого себя.
                    self.session = self.application.AuthorizedPool[login]

                    # Прогрузка данных пользователя (self.session)
                    yield self.init_self_user()

                    # Генерируем идентификатор сессии пользователя
                    self.sid = hmac.new(key=bytes(user["password"], 'utf-8'), msg=bytes(self.token, 'utf-8'),
                                        digestmod=hashlib.sha224).hexdigest()

                    # Удаляем токен, он больше нам не нужен.
                    del self.token

                    # Добавляем авторизованное устройство в множество авторизованных устройств
                    self.session.connections[self.device_id] = self
                    # Если другие устройства небыли онлайн "до нас", то уведомляем всех о нашем входе.
                    if len(self.session.connections) == 1:
                        self.session.data["status"] = "online"
                        self.send_updates({
                            "type": "changed_user_data",
                            "login": self.session.data['login'],
                            "update": {
                                "status":"online"
                            }
                        })
                else:
                    status = '400 Bad Request'
                    info = 'Название устройства "%s" стало занято за время вашего отключения. Выберите новое.' % (self.device_id,)
            else:
                status = '400 Bad Request'
                info = "Ошибка при попытке поиска пользователя с таким именем."
        else:
            status = '400 Bad Request'
            info = "Время ожидания сессии истекло."

        if helpers.is_request(data):
            req = {
                "type": "request",
                "for": "session_restore",
                "id": data["on_request"],
                "status": status,
            }
            if status[0:3] == '200':
                req['session_id'] = self.sid
                req['user'] = helpers.filtering_user_data(copy.deepcopy(self.session.data))

            if info:
                req["info"] = info

            self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_update_my_data(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
            update = DictSchema(
                login = StringValue(
                    max=255,
                    pattern=VL.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                name = StringValue(max=255),
                email = StringValue(pattern=VL.P_MAIL, max=255),
                current_info = StringValue(max=255)
            )
        )):
        status = "200 OK"
        info = ""

        if "login" in data["update"].keys()\
                and helpers.parse_login(data["update"]["login"], True) != helpers.parse_login(self.session.data['login'], True):

            old_login = self.session.data['login']
            data["update"]['login'] = helpers.recover_full_login(data["update"]['login'])

            try:
                cursor = yield database.getSomeLogin(data["update"]['login'])
            except:
                logger.error("Error check login exists", exc_info=True)
                # Информируем пользователя
                self.write_client({
                    "type": "request",
                    "for": "update_user_data",
                    "id": data["on_request"],
                    "status": "500 Internal Server Error",
                    "info": "Ошибка при попытке проверки занятости логина, попробуйте еще раз",
                })
                return
            else:
                if cursor.fetchone()[0] > 0:
                    self.write_client({
                        "type": "request",
                        "for": "update_user_data",
                        "id": data["on_request"],
                        "status": "400 Bad Request",
                        "info": "Этот логин уже занят"
                    })
                    return
        else:
            old_login = None
            data["update"]["login"] = self.session.data['login']

        # Если что то изменилось, обновляем данные
        if (len(data["update"]) > 0):
            try:
                yield database.setUser(data["update"], search_by_login=old_login)
            except:
                logger.error("Failed to update user data", exc_info=True)
                status = "500 Internal Server Error"
                info = "Ошибка при попытке изменить данные пользователя"
            else:
                # Обновление логина пользователя, если он был сменен
                if old_login is not None:
                    self.application.AuthorizedPool[
                        helpers.parse_skaro_login(data["update"]['login'])[1]
                    ] = self.session
                    del self.application.AuthorizedPool[
                        helpers.parse_skaro_login(old_login)[1]
                    ]
                else:
                    del data["update"]["login"]

                old_login = self.session.data['login']
                self.session.data.update(data["update"])

                # Отправка уведомлений об изменении данных
                send = {
                    "type": "changed_user_data",
                    "update": data["update"]
                }
                # На устройства самому себе
                for device in [x for x in self.session.connections.values() if x.device_id != self.device_id]:
                    device.write_client(send)
                # На устройства тех, у кого пользователь есть в контактах
                send["login"] = old_login
                self.send_updates(send)
        else:
            status = "400 Bad Request"
            info = "Новые данные либо пусты либо совпадают со старыми"

        req = {
            "type": "request",
            "for": "update_my_data",
            "id": data["on_request"],
            "status": status
        }
        if len(info) > 0:
            req['info'] = info

        self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_search_contacts(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
            query = StringValue(required=True, max=255)
        )):

        status = "200 OK"
        info = ""
        res = None

        (protocol, part_of_login_in_protocol) = helpers.parse_login(data["query"])
        if protocol == "skaro":
            (host, part_of_login) = helpers.parse_skaro_login(part_of_login_in_protocol)
            self_login = helpers.parse_login(self.session.data["login"], True)[2][1:]
            if host == config.read("host"):
                try:
                    cursor = yield database.searchContacts(part_of_login[1:], self_login)
                except:
                    logger.error("Error in searchContacts('%s')" % (data["query"],), exc_info=True)
                    status = "500 Internal Server Error"
                    info = "Ошибка при попытке поиска пользователей"
                else:
                    res = cursor.fetchall()
            else:
                # поиск на другом skaro сервере
                pass
        else:
            # поиск пользователя в другом протоколле
            pass

        req = {
            "type": "request",
            "id": data["on_request"],
            "status": status
        }
        if info:
            req["info"] = info
        if res is not None:
            req["result"] = res

        self.write_client(req)



    # Добавляет пользователя в контакты
    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_add_contact(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            send_message = StringValue()
        )):

        # проверка. может этот юзер уже является контактом
        if helpers.recover_full_login(self.session.data["login"]) in self.session.data["contacts"].keys():
            self.write_info(data,
                status="400 Bad Request",
                info="Этот пользователь или конференция уже является вашим контактом"
            )
            return

        status = '200 OK'
        info = ''

        dump = self.session.data['contacts'].copy()
        # Добавление юзера в контакты пользователя
        self.session.data['contacts'].update({data["login"]: {}})

        try:
            yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
        except:
            logger.error("Failed to add user to contacts", exc_info=True)
            status = "500 Internal Server Error"
            info = "Ошибка при попытке изменить данные пользователя"
            self.session.data['contacts'] = dump
        else:
            (protocol, login_in_prl) = helpers.parse_login(data['login'])
            if protocol != "skaro":
                # другие протоколы...
                # может что то сделать нужно
                pass

        user = None
        if status[:3] == "200":
            user = yield self.get_user_data(data['login'])
            if not user:
                status = "400 Bad Request"
                info = "Ошибка при попытке получить данные пользователя"

        if helpers.is_request(data):
            req = {
                "type": "request",
                "for": "add_contact",
                "id": data["on_request"],
                "status": status
            }
            if user:
                req['user'] = user
            if len(info) > 0:
                req['info'] = info

            self.write_client(req)

    # Удаляет пользователя из контактов
    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_del_contact(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            )
        )):

        dump = self.session.data['contacts'].copy()

        # Удаляем пользоватея из контактов
        is_contact = False
        if data["login"] in self.session.data['contacts'].keys():
            del self.session.data['contacts'][data["login"]]
            is_contact = True

        if not is_contact:
            if helpers.is_request(data):
                self.write_client({
                    "type": "request",
                    "for": "del_contact",
                    "id": data["on_request"],
                    "status": "400 Bad Request",
                    "info": "Пользователь не является вашим контактом"
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "400 Bad Request",
                    "info": "Пользователь не является вашим контактом"
                })

        status = '200 OK'
        info = ''

        try:
            yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
        except:
            logger.error("Failed to delete user from contacts", exc_info=True)
            status = "500 Internal Server Error"
            info = "Ошибка при попытке изменить данные пользователя"
            self.session.data['contacts'] = dump
        else:
            (protocol, login_in_protocol) = helpers.parse_login(data['login'])
            if protocol != "skaro":
                # другие протоколы...
                # может что то сделать нужно
                pass

        if status[:3] == "200":
            # Очистка общей истории сообщений
            is_clear = yield self.clear_history(data['login'])
            if is_clear is False:
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось очистить историю переписки с пользователем %s" % (data['login'],)
                })

        if helpers.is_request(data):
            req = {
                "type": "request",
                "for": "del_contact",
                "id": data["on_request"],
                "status": status
            }
            if len(info) > 0:
                req['info'] = info

            self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_get_contacts(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True)
        )):

        contacts = yield self.get_self_contacts()

        if contacts is not False:
            status = '200 OK'
            info = ''
        else:
            contacts = None
            status = "500 Internal Server Error"
            info = "Ошибка при попытке получить контакты"

        req = {
            "type": "request",
            "for": "get_contacts",
            "id": data["on_request"],
            "status": status,
        }
        if info:
            req['info'] = info

        if contacts is not None:
            req['data'] = contacts

        self.write_client(req)


    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_get_user_data(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            )
        )):

        user = yield self.get_user_data(data['login'])

        if user:
            status = '200 OK'
            info = ''
        else:
            user = False
            status = "400 Bad Request"
            info = "Ошибка при попытке получить данные пользователя,\
                    возможно такого пользователя не существует"

        req = {
            "type": "request",
            "for": "get_user_data",
            "id": data["on_request"],
            "status": status,
        }
        if info:
            req['info'] = info
        if user:
            req['data'] = user

        self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_get_history(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(required=True),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            from_conference = BoolValue()
        )):

        status = '200 OK'
        info = ''

        # is_conf - дополнительный аргумент, привилигированного выбора типа сообщения
        # особенно актуально для сторонних протоколов
        is_conf = data.pop("is_conference", None)

        # Определяем вид сообщений(в контексте конференции или лички)
        if (is_conf is not None and is_conf is True) or helpers.is_skaro_conf_login(data["login"]):
            is_conf = True
        else:
            is_conf = False

        messages = False
        try:
            cursor = yield database.getHistory(data['login'], self.session.data['login'], is_conference=is_conf)
        except:
            status = '500 Internal Server Error'
            info = sys.exc_info()[0]
            logger.error("Error getting history", exc_info=True)
        else:
            messages = cursor.fetchall()

        if not messages:
            messages = []

        # TODO: Плохо, но не придумал пока как лучше сделать.
        for i, msg in enumerate(messages):
            messages[i] = dict(messages[i])
            # Настройка для json формата
            messages[i]["date"] = msg["date"].strftime('%Y-%m-%d %H:%M:%S')

            messages[i]["from"] = {
                "name": msg["sent"],  # TODO: нужно выводить имя, а не логин(усовершенствовать запрос надо)
                "login": msg["sent"]
            }

            if is_conf:
                messages[i]["from"]["in_conference"] = helpers.recover_full_login(data["login"], with_symbol="~")

            del messages[i]["sent"]
            del messages[i]["took"]

        req = {
            "type": "request",
            "for": "get_history",
            "id": data["on_request"],
            "history": messages,
            "status": status,
        }
        if info:
            req['info'] = info

        self.write_client(req)

    # Очистка общей истории сообщений
    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_clear_history(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            from_conference = BoolValue()
        )):

        is_clear = yield self.clear_history(data['login'])
        if is_clear is False:
            if helpers.is_request(data):
                self.write_client({
                    "type": "request",
                    "for": "clear_history",
                    "id": data["on_request"],
                    "status": "500 Internal Server Error",
                    "info": "Не удалось очистить историю переписки"
                })
            else:
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось очистить историю переписки с пользователем %s" % (data['login'],)
                })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_send_message(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            to = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            body = StringValue(
                required=True,
                max=1000
            ),
            is_conference = BoolValue(required=False)
        )):

        # status = '200 OK'
        # info = []

        # is_conference - дополнительный аргумент, привилигированного выбора типа сообщения
        # особенно актуально для сторонних протоколов
        is_conf = data.pop("is_conference", None)

        # Определяем вид сообщения(в контексте конференции или лички)
        if (is_conf is not None and is_conf is True) or helpers.is_skaro_conf_login(data["to"]):
            is_conf = True
            (mid, status, info) = yield self.sys_send_conf_message(data)
        else:
            is_conf = False
            (mid, status, info) = yield self.sys_send_message(data)

        # Информируем, что сервер принял сообщение и обработал его.
        if helpers.is_request(data):
            req = {
                "type": "request",
                "for": "send_message",
                "id": data["on_request"],
                "date": data['date'], # added in self.sys_send_(conf)_message(data)
                "status": status,
            }
            if len(info) > 0:
                req['status'] = '500 Internal Server Error'
                req['info'] = "\n".join(info)
            else:
                req["mid"] = mid

            self.write_client(req)

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_add_conference(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            name = StringValue(max=255),
            description = StringValue(max=255),
            members = DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=VL.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    group = StringValue(
                        pattern=VL.compile(r"^[\w\d_]+$", flags=VL.IGNORECASE | VL.UNICODE)
                    )
                )
            ),
            mode = StringValue(
                pattern=VL.compile(r"^[\w\d_]+$", flags=VL.IGNORECASE | VL.UNICODE)
            ),
            password = StringValue(max=255)
        )):

        # Получаем полноценный логин, если он был задан не полностью
        (protocol, login_in_prl) = helpers.parse_login(data["login"], to_recover_with="~")
        data["login"] = helpers.recover_full_login(data["login"], with_symbol="~")

        # Добавление конференции
        if protocol == "skaro":
            (host, skaro_login) = helpers.parse_skaro_login(login_in_prl)
            if host == config.read("host"):
                # Проверка на существование конференции с такимже логином
                try:
                    cursor = yield database.checkConferenceExists(skaro_login)
                except:
                    logger.error("Error checking conference exists", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось проверить существование конференции"
                    )
                    return
                else:
                    if cursor.fetchone()[0] > 0:
                        self.write_info(data,
                            status="400 Bad Request",
                            info="Этот логин конференции уже занят"
                        )
                        return

                # Выставляем права админа автору конференции
                data.setdefault("members", {})[self.session.data["login"]] = {"group": "admin"}

                try:
                    yield database.addConference(data.copy())
                except:
                    logger.error("Error adding conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось добавить конференцию"
                    )
                    return
            else:
                # другие skaro серверы
                pass
        else:
            # создание конференции в других протоколах
            pass

        # Добавляем конференцию в контакты
        self.session.data['contacts'].update({data["login"]: {"type": "conference"}})

        try:
            yield database.setUser({"contacts": self.session.data["contacts"], "login": self.session.data["login"]})
        except:
            logger.error("Failed to add conference to contacts", exc_info=True)
            del self.session.data['contacts'][data["login"]]
            self.write_info(data,
                status="500 Internal Server Error",
                info="Ошибка при попытке изменить данные пользователя"
            )
            if protocol == "skaro" and host == config.read("host"):
                try:
                    yield database.delConference(data["login"])
                except:
                    logger.error("Failed to remove added conference (with errors)", exc_info=True)
                    self.write_client({
                        "type": "error",
                        "status": "500 Internal Server Error",
                        "info": "Не удалось удалить добавленную с ошибками конференцию"
                    })
            return
        else:
            if protocol != "skaro":
                # другие протоколы...
                # может что то сделать нужно
                pass

        if "members" in data.keys():
            # Добавляем новым участникам конференции конференцию в контакты
            try:
                yield database.addContactUsersByLogins(data["add_members"].keys(), {
                    data["login"]: {
                        "type": "conference"
                    }
                })
            except:
                logger.error("Error adding conference in contacts new members", exc_info=True)
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось добавить конференцию в контакты её участникам"
                })
                # todo: тут нужно удалять участников, которым не удалось добавить конфу в контакты
            else:
                self.send_updates({
                    "type": "changed_contacts",
                    "add_contacts": {data["login"]: {"type":"conference"}}
                }, users_for_update=data["add_members"].keys(), send_self_devices=True)

        # Операция прошла успешно
        if helpers.is_request(data):
            self.write_client({
                "type": "request",
                "for": "add_conference",
                "id": data["on_request"],
                "status": "200 OK"
            })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_update_conference(self, data: DictSchema(
            type = StringValue(required=True),
            on_request = IntValue(),
            login = StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            update = DictSchema(
                name = StringValue(max=255),
                description = StringValue(max=255),
                mode = StringValue(
                    pattern=VL.compile(r"^[\w\d_]+$", flags=VL.IGNORECASE | VL.UNICODE)
                ),
                password = StringValue(max=255)
            )
        )):

        # Получаем полноценный логин, если он был задан не полностью
        (protocol, login_in_prl) = helpers.parse_login(data["login"], to_recover_with="~")
        data["login"] = helpers.recover_full_login(data["login"], with_symbol="~")

        # Обновление конференции
        if protocol == "skaro":
            (host, skaro_login) = helpers.parse_skaro_login(login_in_prl)
            if host == config.read("host"):
                # Получение настроек конференции
                try:
                    cursor = yield database.getConference(skaro_login)
                except:
                    logger.error("Error getting conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось получить текущие данные конференции"
                    )
                    return
                else:
                    conf = cursor.fetchone()
                    if not len(conf):
                        self.write_info(data,
                            status="400 Bad Request",
                            info="Такой конференции не существует"
                        )
                        return

                # Проверяем права доступа
                access_error = []
                my_group = conf["members"].get(self.session.data["login"], {}).get("group", "quest")
                conf["login"] = helpers.recover_full_login(conf["login"], with_symbol="~")

                if my_group in ("moderator", "member", "quest"):
                    if "mode" in data["update"].keys() and data["update"]["mode"] != conf["mode"]:
                        access_error.append("Группе \"%s\" запрещено изменять режим конференции".format(my_group))
                    if "password" in data["update"].keys() and data["update"]["password"] != conf["password"]:
                        access_error.append("Группе \"%s\" запрещено изменять/устанавливать пароль конференции"
                                            .format(my_group))

                if my_group in ("member", "quest"):
                    if "name" in data["update"].keys() and data["update"]["name"] != conf["name"]:
                        access_error.append("Группе \"%s\" запрещено изменять название конференции".format(my_group))
                    if "description" in data["update"].keys() and data["update"]["description"] != conf["description"]:
                        access_error.append("Группе \"%s\" запрещено изменять описание конференции".format(my_group))

                if len(access_error):
                    self.write_info(data,
                        status="405 Not Allowed",
                        info="\n".join(access_error)
                    )
                    return

                # Обновляем данные конференции.
                try:
                    yield database.setConference(data["update"].copy())
                except:
                    logger.error("Error updating conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось обновить данные конференции"
                    )
                    return
            else:
                # другие skaro серверы
                conf = {}
                pass
        else:
            # создание конференции в других протоколах
            conf = {}
            pass

        # Извещаем пользователей о изменении конференции
        self.send_updates({
            "type": "changed_conference",
            "login": data["login"],
            "update": data["update"]
        }, for_conference=data["login"])

        # Операция прошла успешно
        if helpers.is_request(data):
            self.write_client({
                "type": "request",
                "for": "update_conference",
                "id": data["on_request"],
                "status": "200 OK"
            })

    @tornado.web.asynchronous
    @tornado.gen.engine
    @VL.validate_args
    def event_change_conference_members(self, data: DictSchema(
            type=StringValue(required=True),
            on_request=IntValue(required=True),
            login=StringValue(
                required=True,
                max=255,
                pattern=VL.P_LOGIN,
                prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
            ),
            add_members=DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=VL.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    group = StringValue(
                        pattern=VL.compile(r"^[\w\d_]+$", flags=VL.IGNORECASE | VL.UNICODE)
                    )
                )
            ),
            del_members=DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=VL.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    reason = StringValue()
                )
            ),
            update_members=DictValue(
                keys = StringValue(
                    required=True,
                    max=255,
                    pattern=VL.P_LOGIN,
                    prepare_callback=lambda x: helpers.parse_login(x, True)[2][1:]
                ),
                values = DictSchema(
                    group = StringValue(
                        pattern=VL.compile(r"^[\w\d_]+$", flags=VL.IGNORECASE | VL.UNICODE)
                    )
                )
            )
        )):

        if "update_members" not in data.keys()\
        and "add_members" not in data.keys()\
        and "del_members" not in data.keys():
            self.write_info(data,
                status="400 Bad Request",
                info="Нужно заполнить хотябы одно из полей: add_members, del_members, update_members"
            )
            return

        # Получаем полноценный логин, если он был задан не полностью
        (protocol, login_in_prl) = helpers.parse_login(data["login"], to_recover_with="~")
        data["login"] = helpers.recover_full_login(data["login"], with_symbol="~")

        # Обновление конференции
        if protocol == "skaro":
            (host, skaro_login) = helpers.parse_skaro_login(login_in_prl)
            if host == config.read("host"):
                # Получение настроек конференции
                try:
                    cursor = yield database.getConference(skaro_login)
                except:
                    logger.error("Error getting conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось получить текущие данные конференции"
                    )
                    return
                else:
                    conf = cursor.fetchone()
                    if not len(conf):
                        self.write_info(data,
                            status="400 Bad Request",
                            info="Такой конференции не существует"
                        )
                        return

                # Проверяем права доступа
                access_error = []
                new_members_list = conf["members"]
                my_group = conf["members"].get(self.session.data["login"], {}).get("group", "quest")
                conf["login"] = helpers.recover_full_login(conf["login"], with_symbol="~")

                if len(data.get("add_members", {})):
                    # Восстанавливаем неполные логины
                    data["add_members"] = {helpers.recover_full_login(m_l): m_i
                                                for m_l, m_i in data["add_members"].items()}

                    for member_login, member_info in data["add_members"].items():

                        new_group = member_info.get("group", "quest")

                        if member_login in conf["members"].keys():
                            access_error.append("Такой участник уже существует")
                            break

                        if my_group in ("moderator", "member", "quest"):
                            if new_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено назначать группу \"%s\""
                                                    .format(my_group, new_group))
                                break

                        if my_group == "quest":
                            pass

                    new_members_list = helpers.recursive_merge(conf["members"], data["add_members"])

                if len(data.get("del_members", {})):
                    # Восстанавливаем неполные логины
                    data["del_members"] = {helpers.recover_full_login(m_l): m_i
                                                for m_l, m_i in data["del_members"].items()}

                    for member_login, member_info in data["del_members"].items():
                        old_group = conf["members"].get(member_login, {}).get("group", "quest")

                        if member_login not in conf["members"].keys():
                            access_error.append("Такой участник не существует")
                            break

                        if my_group in ("moderator", "member", "quest"):
                            if old_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено удалять участников из группы \"%s\""
                                                    .format(my_group, old_group))
                                break

                        if my_group in ("member", "quest"):
                            access_error.append("Группе \"%s\" запрещено удалять участников"
                                                .format(my_group))
                            break

                    new_members_list = {m_l: m_i
                                            for m_l, m_i in conf["members"].items()
                                                if m_l not in data["del_members"].keys()}

                if len(data.get("update_members", {})):
                    # Восстанавливаем неполные логины
                    data["update_members"] = {helpers.recover_full_login(m_l): m_i
                                                for m_l, m_i in data["update_members"].items()}

                    for member_login, member_info in data["update_members"].items():
                        old_group = conf["members"].get(member_login, {}).get("group", "quest")
                        new_group = member_info.get("group", "quest")

                        if member_login not in conf["members"].keys():
                            access_error.append("Такой участник не существует")
                            break

                        if my_group in ("moderator", "member", "quest"):
                            if old_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено изменять участников из группы \"%s\""
                                                    .format(my_group, old_group))
                                break
                            if new_group in ("admin", "moderator"):
                                access_error.append("Группе \"%s\" запрещено назначать группу \"%s\""
                                                    .format(my_group, old_group))
                                break

                        if my_group in ("member", "quest"):
                            access_error.append("Группе \"%s\" запрещено изменять пользователей"
                                                .format(my_group, old_group))
                            break

                    new_members_list = helpers.recursive_merge(conf["members"], data["update_members"])

                if len(access_error):
                    self.write_info(data,
                        status="405 Not Allowed",
                        info="\n".join(access_error)
                    )
                    return

                # Обновляем данные конференции.
                try:
                    yield database.setConference({"members": new_members_list, "login": data["login"]})
                except:
                    logger.error("Error updating conference", exc_info=True)
                    self.write_info(data,
                        status="500 Internal Server Error",
                        info="Не удалось обновить список участников конференции"
                    )
                    return
            else:
                # другие skaro серверы
                conf = {}
                pass
        else:
            # создание конференции в других протоколах
            conf = {}
            pass


        # Извещаем пользователей о изменении конференции
        updates = {
            "type": "changed_conference_members",
            "login": data["login"]
        }

        if len(data.get("add_members", {})):
            # Добавляем новым участникам конференции конференцию в контакты
            try:
                yield database.addContactUsersByLogins(data["add_members"].keys(), {
                    data["login"]: {
                        "type": "conference"
                    }
                })
            except:
                logger.error("Error adding conference in contacts new members", exc_info=True)
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось добавить конференцию в контакты её новым участникам"
                })
                # todo: тут нужно удалять участников, которым не удалось добавить конфу в контакты
            else:
                self.send_updates({
                    "type": "changed_contacts",
                    "add_contacts": {data["login"]: {"type":"conference"}}
                }, users_for_update=data["add_members"].keys(), send_self_devices=True)

            updates["add_members"] = data["add_members"]

        if len(data.get("del_members", {})):
            # Удаляем перечисленных участников конференции
            try:
                yield database.addContactUsersByLogins(data["del_members"].keys(), {
                    data["login"]: None
                })
            except:
                logger.error("Error deleting conference in contacts members", exc_info=True)
                self.write_client({
                    "type": "error",
                    "status": "500 Internal Server Error",
                    "info": "Не удалось удалить конференцию из контактов удаляемых участников"
                })
                # todo: тут нужно добавлять участников, которым не удалось удалить конфу из контактов
            else:
                self.send_updates({
                    "type": "changed_contacts",
                    # todo: set reason from data["del_members"][i]["reason"]
                    "del_contacts": {data["login"]: {"reason":"Some reason"}}
                }, users_for_update=data["del_members"].keys(), send_self_devices=True)

            updates["del_members"] = data["del_members"]

        if len(data.get("update_members", {})):
            updates["update_members"] = data["update_members"]

        self.send_updates(updates, for_conference=data["login"])

        # Операция прошла успешно
        if helpers.is_request(data):
            self.write_client({
                "type": "request",
                "for": "change_conference_members",
                "id": data["on_request"],
                "status": "200 OK"
            })



class WSHandlerServer(WSHandler):
    def on_message(self, message):
        data = self.read_client(message)
        logger.info("got message from server %r", data)


if __name__ == "__main__":
    # add configuration of server from command line(stored in options)
    parse_command_line()

    if config.read("host") is None:
        print('Please set host in config')
        sys.exit(1)

    # Run server
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
