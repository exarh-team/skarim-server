--
-- База данных: `skaro`
--
--
-- Вычисляет коэффициент похожести строк, по алгоритму Танимото(не учитывается порядок символов)
--
CREATE OR REPLACE FUNCTION tanimoto_cmp_strings (a varchar, b varchar)
RETURNS double precision AS
$$
DECLARE
  c int := 0;
  a_ varchar[] := regexp_split_to_array(a, '');
  b_ varchar[] := regexp_split_to_array(b, '');
BEGIN
    FOR i IN 0 .. array_length(a_, 1) LOOP
        IF array[a_[i]] <@ b_::varchar[] THEN
            c := c + 1;
        END IF;
    END LOOP;
    
    RETURN c::double precision / (char_length(a) + char_length(b) - c)::double precision;
END
$$
LANGUAGE plpgsql;
-----------------------------------------------------------

--
-- Обьединяет два json обекта. Передать null равнозначно удалению ключа.
--
CREATE FUNCTION jsonb_merge(JSONB, JSONB) 
RETURNS JSONB AS $$
WITH json_union AS (
    SELECT * FROM JSONB_EACH($1)
    UNION ALL
    SELECT * FROM JSONB_EACH($2)
) SELECT JSON_OBJECT_AGG(key, value)::JSONB
     FROM json_union
     WHERE key NOT IN (SELECT key FROM json_union WHERE value ='null');
$$ LANGUAGE SQL;
-- --------------------------------------------------------

--
-- Структура таблицы `history`
--
CREATE SEQUENCE history_mid_seq;
CREATE TABLE history (
    mid integer NOT NULL DEFAULT nextval('history_mid_seq') PRIMARY KEY,
    sent varchar NOT NULL,
    took varchar NOT NULL,
    date timestamp NOT NULL,
    body text NOT NULL,
    removed varchar[2] NOT NULL DEFAULT '{}'::varchar[2]
);
ALTER SEQUENCE history_mid_seq OWNED BY history.mid;
-- --------------------------------------------------------

--
-- Структура таблицы `history_confs`
--
CREATE SEQUENCE history_confs_mid_seq;
CREATE TABLE history_confs (
    mid integer NOT NULL DEFAULT nextval('history_confs_mid_seq') PRIMARY KEY,
    sent varchar NOT NULL,
    took varchar NOT NULL,
    date timestamp NOT NULL,
    body text NOT NULL,
    removed varchar[] NOT NULL DEFAULT '{}'::varchar[]
);
ALTER SEQUENCE history_confs_mid_seq OWNED BY history_confs.mid;
-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE users (
    login varchar NOT NULL UNIQUE PRIMARY KEY,
    password varchar NOT NULL,
    name varchar NOT NULL default '',
    email varchar NOT NULL default '',
    current_info varchar NOT NULL default '',
    contacts json NOT NULL default '{}',
    accounts json NOT NULL default '{}'
);
-- --------------------------------------------------------

--
-- Структура таблицы `conferences`
--
CREATE TYPE conference_mode AS ENUM ('public', 'by_invite', 'by_password');
CREATE TABLE conferences (
    login varchar NOT NULL UNIQUE PRIMARY KEY,
    name varchar NOT NULL default '',
    description varchar NOT NULL default '',
    members json NOT NULL default '{}',
    password varchar,
    mode conference_mode NOT NULL default 'public'
);
-- --------------------------------------------------------

--
-- Структура таблицы `invites`
--

CREATE TABLE invites (
    invite varchar NOT NULL UNIQUE PRIMARY KEY,
    from_user varchar NOT NULL,
    active boolean default true
);

--
-- Дамп данных таблицы `users`
--
--pgsql
INSERT INTO users (login, password, name, email, current_info, contacts, accounts) VALUES
('modos189', '123', 'Александр Данилов', 'skaro@modos189.ru', 'Гений, миллионер, плейбой, филантроп', '{}', '{}'),
('MrBoriska', '123', 'Борис Лапин', 'test@test.tt', 'идеалист', '{"skaro::skar.im/@modos189":{},"xmpp::jabber@modos189.ru":{}}', '{"xmpp::mrboriska@jabber.ru":{"password":"parolll"}}');
